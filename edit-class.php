<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['Update']))
{
$sid=intval($_GET['classid']);
$Sectionid=$_POST['Sectionid'];
$var = explode("/", $Sectionid);
$Sectionid=$var[0];
$Subjectid=$_POST['Subjectid'];
$var2 = explode("/", $Subjectid);
$Subjectid=$var2[0];
$Semester=$_POST['Semester'];
$SchoolYear=$_POST['SchoolYear'];
$sql="update tblclasses set Sectionid=:Sectionid,Subjectid=:Subjectid,Semester=:Semester,SchoolYear=:SchoolYear where id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':Sectionid',$Sectionid,PDO::PARAM_STR);
$query->bindParam(':Subjectid',$Subjectid,PDO::PARAM_STR);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$msg="Class Info updated successfully";
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Update Class </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Update Class</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Classes</li>
                                        <li class="active">Update Class</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Update Class</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">

<?php
$sid=intval($_GET['classid']);
$getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
$getSection = "IFNULL((SELECT CONCAT(co.CourseCode,'-',i.Year,' ',i.Section) from tblsection i, tblcourses co where i.Courseid=co.c_id and i.id=c.Sectionid limit 1), 'N/A') as Section";
$getCourseid = "IFNULL((SELECT co.c_id from tblsection i, tblcourses co where i.Courseid=co.c_id and i.id=c.Sectionid limit 1), 'N/A') as Courseid";
$getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
$sql = "SELECT c.*, ".$getInstructor.", ".$getSection.", ".$getSubject.", ".$getCourseid." from tblclasses c where c.id=:sid";
$query = $dbh->prepare($sql);
$query->bindParam(':sid',$sid,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>                                               
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Course and Section</label>
                                                        <div class="col-sm-10">
                                                        <input list="Sections" value="<?php echo htmlentities($result->Courseid . '/' .$result->Section) ?>" name="Sectionid" id="default" class="form-control" placeholder="Course and Section" required>
                                                        <datalist id="Sections">
<?php $sql = "SELECT s.*, CONCAT(c.CourseCode,'-',s.Year,' ',s.Section) as CourseSection from tblsection s, tblcourses c where s.Courseid=c.c_id order by c.CourseCode asc, s.Section asc";
$query = $dbh->prepare($sql);
$query->execute();
$results2=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results2 as $result2)
{   ?>
<option value="<?php echo htmlentities($result2->id . '/' .$result2->CourseSection); ?>"><?php echo htmlentities($result2->CourseSection); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Assign Subject</label>
                                                        <div class="col-sm-10">
                                                        <input list="SubjectNames" value="<?php echo htmlentities($result->Subjectid . '/' .$result->SubjectName) ?>" name="Subjectid" id="default" class="form-control" placeholder="Subject" required>
                                                        <datalist id="SubjectNames">
<?php $sql = "SELECT * from tblsubjects order by SubjectName";
$query = $dbh->prepare($sql);
$query->execute();
$results3=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results3 as $result3)
{   ?>
<option value="<?php echo htmlentities($result3->id . '/' .$result3->SubjectName); ?>"><?php echo htmlentities($result3->SubjectName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Instructor</label>
                                                        <div class="col-sm-10">
 <input type="text" name="SchoolYear" class="form-control" id="default" placeholder="<?php echo htmlentities($result->Instructor);?>" required="required" disabled>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Semester</label>
                                                        <div class="col-sm-10">
                                                            <select name="Semester" class="form-control" id="default" required="required">
                                                                <option value="<?php echo htmlentities($result->Semester); ?>"><?php echo htmlentities($result->Semester); ?></option>
                                                                <option value="First">First</option>
                                                                <option value="Second">Second</option>
                                                                <option value="Summer">Summer</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">School Year</label>
                                                        <div class="col-sm-10">
 <input type="text" name="SchoolYear" class="form-control" id="default" value="<?php echo htmlentities($result->SchoolYear);?>" required="required" maxlength="9">
                                                        </div>
                                                    </div>

                                                    <?php }} ?>

                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-10">
                                                            <button type="submit" name="Update" class="btn btn-primary">Update</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>


<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

        if(isset($_GET['delete_sure']))
        {
        $sql = "Update tblclasses Set Instructorid=0 where id='".$_GET['delete_sure']."'";
        $query = $dbh->prepare($sql);
        $query->execute();
        header("Location: manage-assign-instructor.php"); 
        $msg="Student info deleted successfully";
        }

    if(isset($_POST['submit']))
    {
    $Classid=$_POST['Classid'];
    $Instructorid=$_POST['Instructorid']; 
    $var=explode('/',$Instructorid);
    $Instructorid=$var[0];
    $sql="update tblclasses set Instructorid=:Instructorid where id=:Classid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':Classid',$Classid,PDO::PARAM_STR);
    $query->bindParam(':Instructorid',$Instructorid,PDO::PARAM_STR);
    $query->execute();
    
    $msg="Instructor Assigned Successfully";
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Manage Assigned Instructors</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar-ph.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar-ph.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Manage Assigned Instructors</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-proghead.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Assigned Instructors</li>
            							<li class="active">Manage Assigned Instructors</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             
<?php //var_dump($_SESSION); ?>
                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Assign Instructor</h5>
                                                </div>
                                            </div>
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
<?php if(isset($_GET['delete'])):?>
<div class="alert alert-danger left-icon-alert" role="alert">
 <strong>Are you sure to remove instructor?</strong>
 <a href="manage-assign-instructor.php?delete_sure=<?php echo htmlentities($_GET['delete']);?>"><input class="btn btn-danger" type="button" name="edit" value="YES"></a>
 <a href="manage-assign-instructor.php?"><input class="btn btn-default" type="button" name="edit" value="CANCEL"></a>
 </div
 ><?php endif; ?> 
                                            <div class="panel-body p-20">
                                                <div class="row">
                                                    <form class="form-horizontal" method="post">

                                                    <div class="col-sm-4">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-3 control-label">Class ID</label>
                                                        <div class="col-sm-9">
 <input type="text" name="Classid" value="<?php echo htmlentities($_GET['Classid']); ?>" class="form-control" id="default" placeholder="No Class ID Selected" required="required">
                                                        </div>
</div>   
</div>   

                                                    <div class="col-sm-6">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-4 control-label">Select Instructor</label>
                                                        <div class="col-sm-8">
                                                        <input list="Students" name="Instructorid" id="default" class="form-control" placeholder="Select Instructor" required autocomplete="off">
                                                        <datalist id="Students">
<?php $sql = "SELECT * from tblinstructor order by InstructorFullName";
$query = $dbh->prepare($sql);
$query->bindParam(':Departmentid',$_SESSION['Departmentid'],PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->InstructorID . '/' .$result->InstructorFullName); ?>"><?php echo htmlentities($result->InstructorFullName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>
                                                    </div>

                                                    <div class="col-sm-2">
                                                    <div class="form-group">
                                                            <button type="submit" name="submit" class="btn btn-primary">Assign Instructor</button>
                                                    </div>
                                                    </div>

                                                    </form>
                                                </div>
                                                <hr>
                                                <table id="example" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Class ID</th>
                                                            <th>Section</th>
                                                            <th>Subject Code</th>
                                                            <th>Subject Name</th>
                                                            <th>Units</th>
                                                            <th>Instructor</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php 
$getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
$getSection = "IFNULL((SELECT CONCAT(co.CourseCode,'-',i.Year,' ',i.Section) from tblsection i, tblcourses co where i.Courseid=co.c_id and i.id=c.Sectionid limit 1), 'N/A') as Section";
$getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
$getSubjectC = "IFNULL((Select SubjectCode from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectCode";
$getUnits = "IFNULL((Select units from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as units";
$sql = "SELECT c.*, ".$getInstructor.", ".$getSection.", ".$getSubject.", ".$getSubjectC.", ".$getUnits."  from tblclasses c, tblsection sec, tblcourses cour where c.Sectionid=sec.id and sec.Courseid=cour.c_id and cour.Departmentid=:Departmentid and c.Status=1";
$query = $dbh->prepare($sql);
$query->bindParam(':Departmentid',$_SESSION['Departmentid'],PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<tr>
            <td><?php echo htmlentities($cnt);?></td>
            <td><?php echo htmlentities($result->id);?></td>
            <td><?php echo htmlentities($result->Section);?></td>
            <td><?php echo htmlentities($result->SubjectCode);?></td>
            <td><?php echo htmlentities($result->SubjectName);?></td>
            <td class="text-center"><?php echo htmlentities($result->units);?></td>
            <td><?php echo htmlentities($result->Instructor);?></td>
<td>
<a href="manage-assign-instructor.php?delete=<?php echo htmlentities($result->id); ?>"><input type="button" name="delete" value="Remove"> </a> 
<a href="manage-assign-instructor.php?Classid=<?php echo htmlentities($result->id); ?>"><input type="button" name="assign" value="Assign"> </a> 
<a href="view-student-grades.php?Classid=<?php echo htmlentities($result->id); ?>"><input type="button" name="assign" value="View Grades"> </a> 
</td>
</tr>
<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                                    </tbody>
                                                </table>
                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example').DataTable();

                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>
<?php } ?>


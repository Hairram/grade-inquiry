<div class="left-sidebar bg-black-300 box-shadow ">
    <div class="sidebar-content">
        <div class="user-info closed">
            <img src="http://placehold.it/90/c2c2c2?text=User" alt="John Doe" class="img-circle profile-img">
            <h6 class="title">User</h6>
        
        </div>
        <!-- /.user-info -->

        <div class="sidebar-nav">
            <ul class="side-nav color-gray">
                <li class="nav-header">
                    <span class="">Main Category</span>
                </li>
                <li>
                    <a href="dashboard.php"><i class="fa fa-dashboard"></i> <span>Dashboard</span> </a>
                    
                </li>

                <li><a href="profile.php"><i class="fa fa-user"></i> <span> My Profile</span></a></li>


                 <li class="has-children">
                    <a href="#"><i class="fa fa-university"></i> <span>Departments</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-department.php"><i class="fa fa-bars"></i> <span>Create Department</span></a></li>
                        <li><a href="manage-departments.php"><i class="fa fa fa-server"></i> <span>Manage Departments</span></a></li>
                    </ul>
                </li>

                   <li class="has-children">
                    <a href="#"><i class="fa fa-object-group"></i> <span>Courses</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-course.php"><i class="fa fa-bars"></i> <span>Create Course</span></a></li>
                        <li><a href="manage-courses.php"><i class="fa fa fa-server"></i> <span>Manage Courses</span></a></li>
                    </ul>
                </li>

                 <li class="has-children">
                    <a href="#"><i class="fa fa-file-text"></i> <span>Sections</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-section.php"><i class="fa fa-bars"></i> <span>Create Section</span></a></li>
                        <li><a href="manage-sections.php"><i class="fa fa fa-server"></i> <span>Manage Sections</span></a></li>
                    </ul>
                </li>
               
             

                <li class="has-children">
                    <a href="#"><i class="fa fa-book"></i> <span>Subjects</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-subject.php"><i class="fa fa-bars"></i> <span>Create Subject</span></a></li>
                        <li><a href="manage-subjects.php"><i class="fa fa fa-server"></i> <span>Manage Subjects</span></a></li>
                    </ul>
                </li>


                 <li class="has-children">
                    <a href="#"><i class="fa fa-clipboard"></i> <span>Classes</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-class-try.php"><i class="fa fa-bars"></i> <span>Create Class</span></a></li>
                        <li><a href="manage-classes.php"><i class="fa fa fa-server"></i> <span>Manage Classes</span></a></li>
                    </ul>
                </li>
               
               
                <li class="has-children">
                    <a href="#"><i class="fa fa-users"></i> <span>Manage Accounts</span> <i class="fa fa-angle-right arrow"></i></a>
                    <ul class="child-nav">
                        <li><a href="create-student-try.php"><i class="fa fa-bars"></i> <span>Add Students</span></a></li>
                        <li><a href="add-instructor.php"><i class="fa fa-bars"></i> <span>Add Instructor</span></a></li>
                        <li><a href="add-proghead.php"><i class="fa fa-bars"></i> <span>Add Program Head</span></a></li>
                        <li><a href="manage-students.php"><i class="fa fa fa-server"></i> <span>Manage Students</span></a></li>
                        <li><a href="manage-instructors.php"><i class="fa fa fa-server"></i> <span>Manage Instructors</span></a></li>
                        <li><a href="manage-progheads.php"><i class="fa fa fa-server"></i> <span>Manage Program Head</span></a></li>
                        
                    </ul>
    </li>
    <li class="has-children">
        <a href="#"><i class="fa fa-info-circle"></i> <span>Reports</span> <i class="fa fa-angle-right arrow"></i></a>
        <ul class="child-nav">

        <li><a href="view-inc.php"><i class="fa fa-bars"></i> <span>INC students</span></a></li>
        <li><a href="view-failed-stu.php"><i class="fa fa-bars"></i> <span>Failed students</span></a></li>
        <li><a href="view-pass-stu.php"><i class="fa fa-bars"></i> <span>Passed students</span></a></li>

        </ul>
    
            
    </li>

    <li><a href="lock-semester.php"><i class="fa fa-lock"></i> <span>Lock Semester</span></a></li>
    <li><a href="unlocked-sem.php"><i class="fa fa-unlock"></i> <span>Unlock Semester</span></a></li>

    <li><a href="change-password.php"><i class="fa fa fa-server"></i> <span> Admin Change Password</span></a></li>


        </div>
        <!-- /.sidebar-nav -->
    </div>
    <!-- /.sidebar-content -->
</div>
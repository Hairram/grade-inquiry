<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$Sectionid=$_POST['Sectionid'];
$var = explode("/", $Sectionid);
$Sectionid=$var[0];
$Subjectid=$_POST['Subjectid'];
$var2 = explode("/", $Subjectid);
$Subjectid=$var2[0];
$Semester=$_POST['Semester'];
$SchoolYear=$_POST['SchoolYear'];
$Day=$_POST['Day'];
$DTime=$_POST['DTime'];
$Room=$_POST['Room'];
$sql = "SELECT * from tblclasses where Sectionid=:Sectionid and Semester=:Semester and SchoolYear=:SchoolYear and Subjectid=:Subjectid";
$query = $dbh->prepare($sql);
$query->bindParam(':Sectionid',$Sectionid,PDO::PARAM_STR);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query->bindParam(':Subjectid',$Subjectid,PDO::PARAM_STR);
$query->bindParam(':Day',$Day,PDO::PARAM_STR);
$query->bindParam(':DTime',$DTime,PDO::PARAM_STR);
$query->bindParam(':Room',$Room,PDO::PARAM_STR);
$query->execute();
$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() <= 0)
{
$sql="INSERT INTO  tblclasses (Sectionid,Semester,SchoolYear,Subjectid,Day,DTime,Room) VALUES(:Sectionid,:Semester,:SchoolYear,:Subjectid,:Day,:DTime,:Room)";
$query = $dbh->prepare($sql);
$query->bindParam(':Sectionid',$Sectionid,PDO::PARAM_STR);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query->bindParam(':Subjectid',$Subjectid,PDO::PARAM_STR);
$query->bindParam(':Day',$Day,PDO::PARAM_STR);
$query->bindParam(':DTime',$DTime,PDO::PARAM_STR);
$query->bindParam(':Room',$Room,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Class created successfully";
}
else 
{
$error="Something went wrong. Please try again";
}
}
else
{
$error="Adding forbidden due to duplicate entry of records.";
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Class</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style >
            .multiselect {
  width: 200px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  background-color: #1e90ff;
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Create Class</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Classes</li>
                                        <li class="active">Create Class</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Class Creation</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong> <?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                <div class="form-group">

                                                     <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">School Year</label>
                                                        <div class="col-sm-10">
 <input type="text" name="SchoolYear" class="form-control" id="default" placeholder="e.g. <?php echo ((int)date('Y') - 1) . '-' . date('Y'); ?>" required="required" maxlength="9">
                                                        </div>
                                                    </div>


                                                     <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Semester</label>
                                                        <div class="col-sm-10">
                                                            <select name="Semester" class="form-control" id="default" required="required">
                                                                <option selected="selected" value="First">First</option>
                                                                <option value="Second">Second</option>
                                                                <option value="Summer">Summer</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                        <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Course and Section</label>
                                                        <div class="col-sm-10">
                                                        <input list="Sections" name="Sectionid" id="default" class="form-control" placeholder="Course and Section" required>
                                                        <datalist id="Sections">
<?php $sql = "SELECT s.*, CONCAT(c.CourseCode,'-',s.Year,' ',s.Section) as CourseSection from tblsection s, tblcourses c where s.Courseid=c.c_id order by c.CourseCode asc, s.Section asc";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->id . '/' .$result->CourseSection); ?>"><?php echo htmlentities($result->CourseSection); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Assign Subject</label>
                                                        <div class="col-sm-10">
                                                        <input list="SubjectNames" name="Subjectid" id="default" class="form-control" placeholder="Subject" required>
                                                        <datalist id="SubjectNames">
<?php $sql = "SELECT * from tblsubjects order by SubjectName";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->id . '/' .$result->SubjectName); ?>"><?php echo htmlentities($result->SubjectName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

                                                   

                                                   

                                                    <!--Day-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Day</label>
                                                        <div class="col-sm-10">
                                                            <select name="Day" class="form-control" id="default" required="required">
                                                                <option selected="selected" value="First">Monday</option>
                                                                <option value="Second">Tuesday</option>
                                                                <option value="Summer">Wednesday</option>
                                                                   <option value="Summer">Thursday</option>
                                                                      <option value="Summer">Friday</option>
                                                                         <option value="Summer">Saturday</option>
                                                            </select>
                                                        </div>
                                                    </div>

                                                    <!--Time-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Time</label>
                                                        <div class="col-sm-10">
 <input type="time" name="DTime" class="form-control" id="default" placeholder="e.g. 2:00pm to 7:00pm " required="required" maxlength="50">
                                                        </div>
                                                    </div>


                                                    <!--Room no.-->
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Room</label>
                                                        <div class="col-sm-10">
 <input type="text" name="Room" class="form-control" id="default" placeholder="e.g. Room 209" required="required" maxlength="9">
                                                        </div>
                                                    </div>

                                           

                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-20">
                                                            <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });

            var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
};
        </script>
    </body>
</html>
<?PHP } ?>

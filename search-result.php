<?php
session_start();
error_reporting(0);
include('includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grade Inquiry System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
    <div class="main-wrapper">

<!-- ========== TOP NAVBAR ========== -->
<?php include('includes/topbar.php');?> 
<!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
<div class="content-wrapper">
    <div class="content-container">
<?php include('includes/leftbarStudent.php');?>  

        <div class="main-page">
            <div class="container">
                <div class="row page-title-div">
                    <div class="col-md-6">
                        <h2 class="title">View Grades</h2>
                    
                    </div>
                    
                    <!-- /.col-md-6 text-right -->
                </div>
                <!-- /.row -->
                <div class="row breadcrumb-div">
                    <div class="col-md-6">
                        <ul class="breadcrumb">
                            <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                            <li> View Grades</li>
                        </ul>
                    </div>
                 
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->

                        <section class="section">
                        <div id="printMe">
                                <div class="row">

                                    <div class="col-md-10 col-md-offset-1">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                     
                                                     <h5 class="text-center">Opol Community College<br>
                                                        Poblacion, Opol, Misamis Oriental<br>
                                                        </h5>

<?php
$rollid = $_SESSION['alogin'];
$getCourse = "IFNULL((Select CourseCode from tblcourses i where i.c_id=s.Courseid limit 1), 'N/A') as Course";
$getCourseName = "IFNULL((Select CourseName from tblcourses i where i.c_id=s.Courseid limit 1), 'N/A') as CourseName";
$getYearLevel = "IFNULL((Select a.StudentYearLevel from tblassignstudent a, tblclasses c, tblsection sec where c.Sectionid=sec.id and a.Classid=c.id and s.StudentId=a.Studentid limit 1), '') as StudentYearLevel";
$qery = "SELECT s.*, ".$getCourse.", ".$getYearLevel.", ".$getCourseName." from tblstudents s where s.RollId=:rollid";
$stmt = $dbh->prepare($qery);
$stmt->bindParam(':rollid',$rollid,PDO::PARAM_STR);
$stmt->execute();
$resultss=$stmt->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($stmt->rowCount() > 0) 
{
foreach($resultss as $row)
{   ?>

<b>Student Fullname :</b> <?php echo htmlentities($row->StudentName);?></p>
<p><b>Student ID :</b> <?php echo htmlentities($row->RollId);?>
<p><b>Course Name:</b> <?php echo htmlentities($row->CourseName);?>
<p><b>Course Code:</b> <?php echo htmlentities($row->Course);?>

<?php break;}
 ?>
                                            </div>
                                            <div class="panel-body p-20">
                                            
                                                <table border="1" class="table table-hover table-bordered" style="width:100% !important;">
                                                    <tbody>
<?php                                              
// Code for result
$getCourse = "IFNULL((Select CourseCode from tblcourses i where i.c_id=s.Courseid limit 1), 'N/A') as Course";
$getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
$getSubjectCode = "IFNULL((Select SubjectCode from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectCode";
$getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
$getGrade = "IFNULL((Select marks from tblresult i where i.AssignStudentid=a.id limit 1), 'N/A') as Mark";
$getRemarks = "IFNULL((Select remarks from tblresult i where i.AssignStudentid=a.id limit 1), 'N/A') as Remarks";
$getUnit = "IFNULL((Select units from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as units";
$sql = "SELECT c.SchoolYear, c.Semester, s.*, a.StudentYearLevel, ".$getCourse.", ".$getSubjectCode.", ".$getSubject.", ".$getGrade.", ".$getRemarks.", ".$getUnit.", ".$getInstructor." from tblstudents s, tblassignstudent a, tblclasses c, tblsection sec where c.Sectionid=sec.id and a.Classid=c.id and s.StudentId=a.Studentid and s.RollId=:rollid ORDER BY c.SchoolYear asc, c.Semester asc";
$query= $dbh -> prepare($sql);
$query->bindParam(':rollid',$rollid,PDO::PARAM_STR);
$query-> execute();  
$results = $query -> fetchAll(PDO::FETCH_OBJ);
$cnt=1;

if($query->rowCount()>0)
{ 
foreach($results as $result){
    $schoolyear = $result->SchoolYear;
    $semester = $result->Semester;
    if($result->StudentYearLevel == 1){
        $yrlevel = "1st Year";
    }elseif($result->StudentYearLevel == 2){
        $yrlevel = "2nd Year";
    }elseif($result->StudentYearLevel == 3){
        $yrlevel = "3rd Year";
    }elseif($result->StudentYearLevel == 4){
        $yrlevel = "4th Year";
    }elseif($result->StudentYearLevel == 5){
        $yrlevel = "5th Year";
    }elseif($result->StudentYearLevel == 6){
        $yrlevel = "6th Year";
    }elseif($result->StudentYearLevel == 7){
        $yrlevel = "7th Year";
    }elseif($result->StudentYearLevel == 8){
        $yrlevel = "8th Year";
    }
    break;
}?>
<tr>
       <th colspan="7" style="text-align:center;">School Year: <?php echo htmlentities($schoolyear);?></th>
    </tr>
    <tr>
       <th colspan="7" style="text-align:center;"><?php echo htmlentities($semester) .' Semester (' .$yrlevel.')';?></th>
    </tr>
    <tr>
                                                            <th>No.</th>
                                                            <th>Subject Code</th>
                                                             <th>Descriptive Title</th>    
                                                            <th>Final Grade</th>
                                                            <th>Remarks</th>
                                                            <th>Units</th>
                                                            <th>Instructor</th>

                                                        </tr>
<?php
$total_units = 0;
foreach($results as $result){
?>
<?php if($result->SchoolYear != $schoolyear): ?>
    <?php 
        if($result->StudentYearLevel == 1){
            $yrlevel = "1st Year";
        }elseif($result->StudentYearLevel == 2){
            $yrlevel = "2nd Year";
        }elseif($result->StudentYearLevel == 3){
            $yrlevel = "3rd Year";
        }elseif($result->StudentYearLevel == 4){
            $yrlevel = "4th Year";
        }elseif($result->StudentYearLevel == 5){
            $yrlevel = "5th Year";
        }elseif($result->StudentYearLevel == 6){
            $yrlevel = "6th Year";
        }elseif($result->StudentYearLevel == 7){
            $yrlevel = "7th Year";
        }elseif($result->StudentYearLevel == 8){
            $yrlevel = "8th Year";
        }
    ?>
    <th colspan="5" scope="row" class="text-right" colspan="2">Total Units:</th>           
                                                            <td><?php echo htmlentities($total_units); ?></td>
                                                            <td></td>
                                                             </tr>
    <tr>
       <th colspan="7" style="text-align:center;">School Year: <?php echo htmlentities($result->SchoolYear);?></th>
    </tr>
    <tr>
       <th colspan="7" style="text-align:center;"><?php echo htmlentities($result->Semester) .' Semester (' . $yrlevel.')';?></th>
    </tr>
    <tr>
                                                            <th>No.</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>    
                                                            <th>Final Grade</th>
                                                            <th>Remarks</th>
                                                            <th>Units</th>
                                                            <th>Instructor</th>

                                                        </tr>
    <?php $schoolyear = $result->SchoolYear; ?>
    <?php $semester = $result->Semester; ?>
    <?php $total_units = 0; ?>
    <?php $cnt = 1; ?>
<?php elseif($result->Semester != $semester): ?>
    <?php 
        if($result->StudentYearLevel == 1){
            $yrlevel = "1st Year";
        }elseif($result->StudentYearLevel == 2){
            $yrlevel = "2nd Year";
        }elseif($result->StudentYearLevel == 3){
            $yrlevel = "3rd Year";
        }elseif($result->StudentYearLevel == 4){
            $yrlevel = "4th Year";
        }elseif($result->StudentYearLevel == 5){
            $yrlevel = "5th Year";
        }elseif($result->StudentYearLevel == 6){
            $yrlevel = "6th Year";
        }elseif($result->StudentYearLevel == 7){
            $yrlevel = "7th Year";
        }elseif($result->StudentYearLevel == 8){
            $yrlevel = "8th Year";
        }
    ?>
    <th colspan="5" scope="row" class="text-right" colspan="2">Total Units:</th>           
                                                            <td><?php echo htmlentities($total_units); ?></td>
                                                            <td></td>
                                                             </tr>
    <tr>
       <th colspan="7" style="text-align:center;"><?php echo htmlentities($result->Semester) .' Semester (' .$yrlevel.')';?></th>
    </tr>
    <tr>
                                                            <th>No.</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>    
                                                            <th>Final Grade</th>
                                                            <th>Remarks</th>
                                                            <th>Units</th>
                                                            <th>Instructor</th>

                                                        </tr>
    <?php $semester = $result->Semester; ?>
    <?php $total_units = 0; ?>
    <?php $cnt = 1; ?>
<?php endif; ?>

                                            <tr>
                                                <th scope="row"><?php echo htmlentities($cnt);?></th>
                                                <td><?php echo htmlentities($result->SubjectCode);?></td>
                                                <td><?php echo htmlentities($result->SubjectName);?></td>
                                                <td class="text-center"><?php echo htmlentities($result->Mark);?></td>
                                                <td><?php echo htmlentities($result->Remarks);?></td>     
                                                <td><?php echo htmlentities($result->units);?></td>               
                                                <td><?php echo htmlentities($result->Instructor);?></td>
                                            </tr>
<?php 
$total_units+=$result->units;
$cnt++;}
?>

                                                             <tr>
                                                <th colspan="5" scope="row" class="text-right" colspan="2">Total Units:</th>           
                                                            <td><?php echo htmlentities($total_units); ?></td>
                                                            <td></td>
                                                             </tr>
<tr>
                                                

 <?php } else { ?>     
<div class="alert alert-warning left-icon-alert" role="alert">
                                            <strong>Notice!</strong> No Subjects Found.
 <?php }
?>
                                        </div>
 <?php 
 } else
 {?>

<div class="alert alert-danger left-icon-alert" role="alert">
strong>Oh snap!</strong>
<?php
echo htmlentities("Invalid Roll Id");
 }
?>
                                        </div>



                                                    </tbody>
                                                </table>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                        </div>
                                        </div>
                                        <div class="row text-right"><div class="col-sm-10 offset-1"><a class="btn btn-primary printMeButton">Print this page</a></div></div>
                                        <!-- /.panel -->
                                    </div>
                                    
                                    <!-- /.col-md-6 -->
<?php  ?>

  
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                
            });
            $(document).on("click", ".printMeButton", function () {
                console.log('here');
                var printContents = document.getElementById("printMe").innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

    </body>
</html>

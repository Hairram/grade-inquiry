<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$ClassId=$_POST['ClassId'];
$var=explode('/',$ClassId);
$ClassId=$var[0];
$SubjectId=$_POST['SubjectId']; 
$var2=explode('/',$SubjectId);
$SubjectId=$var2[0];
$StudentId=$_POST['StudentId']; 
$var3=explode('/',$StudentId);
$StudentId=$var3[0];
$InstructorID=$_POST['InstructorID']; 
$var4=explode('/',$InstructorID);
$InstructorID=$var4[0];
$Semester=$_POST['Semester']; 
$SchoolYear=$_POST['SchoolYear']; 
$status=1;
$sql = "SELECT * from tblsubjectcombination where ClassId=:ClassId and SubjectId=:SubjectId and StudentId=:StudentId and InstructorID=:InstructorID and Semester=:Semester and SchoolYear=:SchoolYear";
$query = $dbh->prepare($sql);
$query->bindParam(':ClassId',$ClassId,PDO::PARAM_INT);
$query->bindParam(':SubjectId',$SubjectId,PDO::PARAM_INT);
$query->bindParam(':StudentId',$StudentId,PDO::PARAM_INT);
$query->bindParam(':InstructorID',$InstructorID,PDO::PARAM_INT);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query->execute();
$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() <= 0)
{
$sql="INSERT INTO  tblsubjectcombination(ClassId,SubjectId,status,StudentId,InstructorID,Semester,SchoolYear) VALUES(:ClassId,:SubjectId,:status,:StudentId,:InstructorID,:Semester,:SchoolYear)";
$query = $dbh->prepare($sql);
$query->bindParam(':ClassId',$ClassId,PDO::PARAM_STR);
$query->bindParam(':SubjectId',$SubjectId,PDO::PARAM_STR);
$query->bindParam(':status',$status,PDO::PARAM_STR);
$query->bindParam(':StudentId',$StudentId,PDO::PARAM_STR);
$query->bindParam(':InstructorID',$InstructorID,PDO::PARAM_STR);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Class assigned successfully";
}
else 
{
$error="Something went wrong. Please try again";
}
}
else
{
$error="Record already exists. ".$var3[1].'/'.$var[1].'/Subject '.$var2[1]." in ".$Semester.' Semester/'.$SchoolYear.'/Instructor '.$var4[1];
}
}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Create Class </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Assign Class</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Classes</li>
                                        <li class="active">Assign Class</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Class Assignment</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong> <?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Course and Year</label>
                                                        <div class="col-sm-10">
                                                        <input list="ClassNames" name="ClassId" id="default" class="form-control" placeholder="Course and Year" required>
                                                        <datalist id="ClassNames">
<?php $sql = "SELECT * from tblclasses order by ClassName";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->id . '/' . $result->ClassName . ' Section-' . $result->Section); ?>"><?php echo htmlentities($result->ClassName); ?>&nbsp; Section-<?php echo htmlentities($result->Section); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>
<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Assign Subject</label>
                                                        <div class="col-sm-10">
                                                        <input list="SubjectNames" name="SubjectId" id="default" class="form-control" placeholder="Subject" required>
                                                        <datalist id="SubjectNames">
<?php $sql = "SELECT * from tblsubjects order by SubjectName";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->id . '/' .$result->SubjectName); ?>"><?php echo htmlentities($result->SubjectName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>
                                                
<div class="form-group">
                                        <label for="default" class="col-sm-2 control-label">Assign Student</label>
                                                        <div class="col-sm-10">
                                                        <input list="Students" name="StudentId" id="default" class="form-control" placeholder="Student" required>
                                                        <datalist id="Students">
<?php $sql = "SELECT * from tblstudents order by StudentName";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->StudentId . '/' . $result->StudentName); ?>"><?php echo htmlentities($result->StudentName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

<div class="form-group">
                                        <label for="default" class="col-sm-2 control-label">Assign Instructor</label>
                                                        <div class="col-sm-10">
                                                        <input list="Instructors" name="InstructorID" id="default" class="form-control" placeholder="Instructor" required>
                                                        <datalist id="Instructors">
<?php $sql = "SELECT * from tblinstructor order by InstructorFullName";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->InstructorID . '/' . $result->InstructorFullName); ?>"><?php echo htmlentities($result->InstructorFullName); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Semester</label>
                                                        <div class="col-sm-10">
                                                            <select name="Semester" class="form-control" id="default" required="required">
                                                                <option selected="selected" value="First">First</option>
                                                                <option value="Second">Second</option>
                                                                <option value="Summer">Summer</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">School Year</label>
                                                        <div class="col-sm-10">
 <input type="text" name="SchoolYear" class="form-control" id="default" value="<?php echo ((int)date('Y') - 1) . '-' . date('Y'); ?>" required="required" maxlength="9">
                                                        </div>
                                                    </div>

                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-20">
                                                            <button type="submit" name="submit" class="btn btn-primary">Assign</button>
                                                        </div>
                                                    </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>


<?php
$connect = mysqli_connect("localhost", "root", "", "srms_final");
$output = '';
if(isset($_POST["import"]))
{
    // $extension = end(explode(".", $_FILES["excel"]["name"])); // For getting Extension of selected file
    $value = explode(".", $_FILES["excel"]["name"]);
    $extension = strtolower(array_pop($value));
    $allowed_extension = array("csv"); //allowed extension
    if(in_array($extension, $allowed_extension)) //check selected file extension is present in allowed extension array
    {
    $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
    include("../OccGradeInquiry /Classes/PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code

    $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
    $highestRow = $worksheet->getHighestRow();
    for($row=2; $row<=$highestRow; $row++)
    {
    $a = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
    $b = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
    $c = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
    $query_delete = "DELETE FROM tblresult where AssignStudentid=(Select a.id FROM tblassignstudent a, tblstudents s WHERE s.StudentId=a.Studentid and a.Classid=".$_GET['Classid']." and s.RollId='".$a."')";
    mysqli_query($connect, $query_delete);
        if($c == 'INC'){
            $INC = 1;
        }else{
            $INC = 0;
        }
    $query = "INSERT INTO tblresult(AssignStudentid,marks,INC,remarks) VALUES ((Select a.id FROM tblassignstudent a, tblstudents s WHERE s.StudentId=a.Studentid and a.Classid=".$_GET['Classid']." and s.RollId='".$a."'), '".$b."', '".$INC."', '".$c."')";
    mysqli_query($connect, $query);
    }
    }
    $output .= "<label class='text-success'>Imported Successfully!</label>";
    }
    else
    {
    $output = '<label class="text-danger">Invalid File</label>'; //if non excel file then
    }
}
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
        if(isset($_GET['Classid'])){
            $Classid_g = $_GET['Classid'];
            $getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
            $getSection = "IFNULL((SELECT CONCAT(co.CourseCode,'-',i.Year,' ',i.Section) from tblsection i, tblcourses co where i.Courseid=co.c_id and i.id=c.Sectionid limit 1), 'N/A') as Section";
            $getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
            $sql = "SELECT c.*, ".$getInstructor.", ".$getSection.", ".$getSubject." from tblclasses c where c.id=:stid";
            $query = $dbh->prepare($sql);
            $query->bindParam(':stid',$Classid_g,PDO::PARAM_STR);
            $query->execute();
            $results=$query->fetchAll(PDO::FETCH_OBJ);
            foreach($results as $result){
                $Section = $result->Section;
                $Subject = $result->SubjectName;
                $SchoolYear = $result->SchoolYear;
                $Semester = $result->Semester;
            }
        }else{
            header("Location: manage-assigned-class.php"); 
        }
    if(isset($_GET['setINC'])){
        $setINC=$_GET['setINC']; 
        $sql = "Select * FROM tblresult where AssignStudentid=:setINC";
        $query = $dbh->prepare($sql);
        $query->bindParam(':setINC',$setINC,PDO::PARAM_STR);
        $query->execute();
        $query->fetchAll(PDO::FETCH_OBJ);
        if($query->rowCount() > 0)
        {
        $sql="update tblresult set INC=1,remarks='INC' where AssignStudentid=:setINC";
        $query = $dbh->prepare($sql);
        $query->bindParam(':setINC',$setINC,PDO::PARAM_STR);
        $query->execute();
        }else{
        $sql="INSERT INTO  tblresult(AssignStudentid,remarks,INC) VALUES(:setINC,'INC','1')";
        $query = $dbh->prepare($sql);
        $query->bindParam(':setINC',$setINC,PDO::PARAM_STR);
        $query->execute();
        $dbh->lastInsertId();
        }
        $msg="Student set as INC";
        header("Location: manage-student-grades.php?Classid=".$Classid_g); 
    }
    if(isset($_GET['setComplete'])){
        $setComplete=$_GET['setComplete']; 
        $sql="update tblresult set INC=0,remarks='' where AssignStudentid=:setComplete";
        $query = $dbh->prepare($sql);
        $query->bindParam(':setComplete',$setComplete,PDO::PARAM_STR);
        $query->execute();
        $msg="Student set as Completed";
        header("Location: manage-student-grades.php?Classid=".$Classid_g); 
    }
    if(isset($_GET['DeclareResult'])){
        $sql = "SELECT r.* from tblassignstudent a, tblresult r where a.id=r.AssignStudentid and a.Classid=:Classid";
        $query = $dbh->prepare($sql);
        $query->bindParam(':Classid',$_GET['Classid'],PDO::PARAM_STR);
        $query->execute();
        $results=$query->fetchAll(PDO::FETCH_OBJ);
        if($query->rowCount() > 0)
        {
        foreach($results as $result)
        {
            $remarks = $result->remarks;
            if($result->remarks != 'INC'){
                if($result->marks < 3.1){
                    $remarks = 'PASSED';
                }else{
                    $remarks = 'FAILED';
                }
            }
            $sql="update tblresult set remarks=:remarks where id=:rid";
            $query = $dbh->prepare($sql);
            $query->bindParam(':rid',$result->id,PDO::PARAM_STR);
            $query->bindParam(':remarks',$remarks,PDO::PARAM_STR);
            $query->execute();
        }
        }
        header("Location: manage-student-grades.php?Classid=".$Classid_g); 
    }
    if(isset($_POST['submit']))
    {
    $Classid=$Classid_g;
    $AssignStudentid=$_POST['AssignStudentid']; 
    $marks=$_POST['marks']; 
    if($marks < 3.1){
        $remarks = 'PASSED';
    }else{
        $remarks = 'FAILED';
    }
    $sql = "Select * FROM tblresult where AssignStudentid=:AssignStudentid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':AssignStudentid',$AssignStudentid,PDO::PARAM_STR);
    $query->execute();
    $results=$query->fetchAll(PDO::FETCH_OBJ);
    if($query->rowCount() > 0)
    {
    $Year=$_POST['Year']; 
    $sql="update tblresult set marks=:marks,remarks=:remarks where AssignStudentid=:AssignStudentid";
    $query = $dbh->prepare($sql);
    $query->bindParam(':marks',$marks,PDO::PARAM_STR);
    $query->bindParam(':AssignStudentid',$AssignStudentid,PDO::PARAM_STR);
    $query->bindParam(':remarks',$remarks,PDO::PARAM_STR);
    $query->execute();
    $msg="Grade updated successfully";
    }
    else
    {
    $sql="INSERT INTO  tblresult(AssignStudentid,marks,remarks) VALUES(:AssignStudentid,:marks,:remarks)";
    $query = $dbh->prepare($sql);
    $query->bindParam(':AssignStudentid',$AssignStudentid,PDO::PARAM_STR);
    $query->bindParam(':marks',$marks,PDO::PARAM_STR);
    $query->bindParam(':remarks',$remarks,PDO::PARAM_STR);
    $query->execute();
    $lastInsertId = $dbh->lastInsertId();
    if($lastInsertId)
    {
    $msg="Grade Added Successfully";
    }
    else 
    {
    $error="Something went wrong. Please try again";
    }
    }
    }
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin Manage Assigned Students</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar-ins.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar1.php');?>  

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="row">
            <div class="col-sm-10">
                <h4 class="modal-title" id="exampleModalLabel">ADD GRADE</h4>
            </div>
            <div class="col-sm-2">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        
        
      </div>
      <div class="modal-body">
      <form class="row" method="post">
                                                    <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-5 control-label">Assigned Student ID</label>
                                                        <div class="col-sm-7">
 <input type="text" name="AssignStudentid" class="form-control" id="AssignStudentid" placeholder="No Assigned Student Selected" required="required" readonly>
                                                        </div>
</div>   
</div>   

<div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-5 control-label">Mark</label>
                                                        <div class="col-sm-7">
 <input type="text" name="marks" class="form-control" id="default" placeholder="Enter Mark" required="required">
                                                        </div>
</div>   
</div>   

                                                    <div class="col-sm-12 text-left">
                                                    
                                                    </div>
                                                
      </div>
      <div class="modal-footer">
      <div class="form-group">
                                                            <button type="submit" name="submit" class="btn btn-primary">Add Grade</button>
                                                    </div>
                                                    </form> 
      </div>
    </div>
  </div>
</div>
<!-- /Modal -->

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Manage Assigned Students</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-instructor2.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Assigned Students</li>
            							<li class="active">Manage Assigned Students</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">

                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5><u><?php echo htmlentities($Section.'/ '.$Subject); ?></u> (S.Y. <?php echo htmlentities($SchoolYear.'/ '.$Semester.' Semester'); ?>)</h5>
                                                </div>
                                            </div>
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
<?php if(isset($_GET['delete'])):?>
<div class="alert alert-danger left-icon-alert" role="alert">
 <strong>Are you sure to delete student <?php echo $_GET['delete']; ?>?</strong>
 <a href="manage-assign-students.php?Classid=<?php echo htmlentities($Classid_g); ?>&delete_sure=<?php echo htmlentities($_GET['delete']);?>"><input class="btn btn-danger" type="button" name="edit" value="YES"></a>
 <a href="manage-assign-students.php?Classid=<?php echo htmlentities($Classid_g); ?>"><input class="btn btn-default" type="button" name="edit" value="CANCEL"></a>
 </div
 ><?php endif; ?> 
                                            <div class="panel-body p-20">
                                                <div class="row">
                                                
                                                </div>
                                                <div class="row">
                                                <form method="post" enctype="multipart/form-data">
                                                    <div class="col-sm-5 text-right"><i class="text-danger">Upload .csv (Student Roll id, Student Fullname, Mark, Remarks)</i></div>
                                                    <div class="col-sm-5">
                                                        <div class="">
                                                            <input type="file" accept=".csv" name="excel" class="margin-input-bot form-control" />
                                                        </div>
                                                    </div>
                                                    <div class="col-sm-2 text-left">
                                                        <input type="submit" name="import" class="btn btn-info" value="Import" />
                                                    </div>
                                                    <div class="col-sm-12 text-right">
                                                        <?php echo $output; ?>
                                                    </div>
                                                </form>
                                                <div class="row">
                                                    <div class="col-sm-11 col-sm-offset-1">
                                                    <!-- <a href="manage-student-grades.php?Classid=<?php echo htmlentities($Classid_g); ?>&DeclareResult=true"><input type="button" class="btn btn-primary" name="assign" value="Declare Result"> </a>  -->
                                                    </div>
                                                </div>
                                                </div>
                                                <hr>
                                                    <div class="panel-title">
                                                        <h5>List of Students:</h5>
                                                    </div>
                                                    <br>
                                                <table id="example1" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Roll ID</th>
                                                            <th>Student Fullname</th>
                                                            <th>Course</th>
                                                            <th>Mark</th>
                                                            <th>Remarks</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>

<?php 
$getCourse = "IFNULL((SELECT co.CourseCode from tblcourses co where s.Courseid=co.c_id limit 1), 'N/A') as Course";
$getMarks = "IFNULL((SELECT r.marks from tblresult r where r.AssignStudentid=a.id limit 1), 'N/A') as Mark";
$getRemarks = "IFNULL((SELECT r.remarks from tblresult r where r.AssignStudentid=a.id limit 1), 'N/A') as Remark";
$getINC = "IFNULL((SELECT r.INC from tblresult r where r.AssignStudentid=a.id limit 1), 0) as INC";
$getIDINC = "IFNULL((SELECT r.id from tblresult r where r.AssignStudentid=a.id limit 1), 0) as INCid";
$sql = "SELECT s.*, a.id as AssignStudentid, a.StudentYearLevel, ".$getCourse.", ".$getMarks.", ".$getRemarks.", ".$getINC.", ".$getIDINC.", a.CreationDate from tblassignstudent a, tblstudents s where s.StudentId=a.Studentid and a.Classid=:Classid order by s.StudentName";
$query = $dbh->prepare($sql);
$query->bindParam(':Classid',$Classid_g,PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<tr>
    <td><?php echo htmlentities($cnt);?></td>
 <td><?php echo htmlentities($result->RollId);?></td>
                                                            <td><?php echo htmlentities($result->StudentName);?></td>
                                                            <td><?php echo htmlentities($result->Course.'-'.$result->StudentYearLevel);?></td>
                                                            <td><?php echo htmlentities($result->Mark);?></td>
                                                            <td><?php echo htmlentities($result->Remark);?></td>
<td>
<a href="#exampleModal" data-id="<?php echo htmlentities($result->AssignStudentid); ?>" class="exampleModal"><input type="button" name="Add Class" value="Add Grade" data-toggle="modal" data-target="#exampleModal"> </a> 
<?php if($result->INC == 1): ?>
<a href="manage-student-grades.php?Classid=<?php echo htmlentities($Classid_g); ?>&setComplete=<?php echo htmlentities($result->AssignStudentid);?>"><input type="button" name="setComplete" value="Set as COMPLETED"> </a> 
<?php else: ?>
<a href="manage-student-grades.php?Classid=<?php echo htmlentities($Classid_g); ?>&setINC=<?php echo htmlentities($result->AssignStudentid);?>"><input type="button" name="setINC" value="Set as INC"> </a> 
<?php endif; ?>
</td>
</tr>
<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $('#example1').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });

            $(document).on("click", ".exampleModal", function () {
                var passID = $(this).data('id');
                console.log('here');
                $(".modal-body #AssignStudentid").val( passID );
                // As pointed out in comments, 
                // it is unnecessary to have to manually call the modal.
                // $('#addBookDialog').modal('show');
            });
        </script>
    </body>
</html>
<?php } ?>


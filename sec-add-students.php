<?php
$connect = mysqli_connect("localhost", "root", "", "srms_final");
$output = '';
if(isset($_POST["import"]))
{
    // $extension = end(explode(".", $_FILES["excel"]["name"])); // For getting Extension of selected file
    $value = explode(".", $_FILES["excel"]["name"]);
    $extension = strtolower(array_pop($value));
    $allowed_extension = array("csv"); //allowed extension
    if(in_array($extension, $allowed_extension)) //check selected file extension is present in allowed extension array
    {
    $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
    include("../OccGradeInquiry /Classes/PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code

    $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
    $highestRow = $worksheet->getHighestRow();
    for($row=2; $row<=$highestRow; $row++)
    {
    $a = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
    $b = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
    $c = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
    $d = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(3, $row)->getValue());
    $e = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(4, $row)->getValue());
    $f = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(5, $row)->getValue());
    $g = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(6, $row)->getValue());
    $var = explode("/",$f);
    if(count($var) == 3){
        $f = $var[2] . '-' . $var[0] . '-' . $var[1];
    }
    $query = "Select * FROM tblstudents where RollId='".$b."'";
    $result = mysqli_num_rows(mysqli_query($connect, $query));
    if($result == 0){
        $query = "INSERT INTO tblstudents(StudentName,RollId, Address, StudentEmail,Gender,DOB,Courseid,Status,Password) VALUES ('".$a."', '".$b."', '".$c."', '".$d."', '".$e."', '".$f."', '".$g."', 1, '".$b."')";
        mysqli_query($connect, $query);
    }
    }
    }
    $output .= "<label class='text-success'>Imported Successfully!</label>";
    }
    else
    {
    $output = '<label class="text-danger">Invalid File</label>'; //if non excel file then
    }
}
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{

if(isset($_POST['submit']))
{
$StudentName=$_POST['StudentName'];
$RollId=$_POST['RollId']; 
$Address=$_POST['Address'];
$StudentEmail=$_POST['StudentEmail']; 
$Gender=$_POST['Gender']; 
$DOB=$_POST['DOB']; 
$Status=$_POST['Status'];
$Courseid=$_POST['Courseid'];
$var=explode('/',$Courseid);
$Courseid=$var[0];
$sql="INSERT INTO  tblstudents(StudentName,RollId,Address,StudentEmail,Gender,DOB,Status,Courseid,Password) VALUES(:StudentName,:RollId,:Address,:StudentEmail,:Gender,:DOB,:Status,:Courseid,:RollId)";
$query = $dbh->prepare($sql);
$query->bindParam(':StudentName',$StudentName,PDO::PARAM_STR);
$query->bindParam(':RollId',$RollId,PDO::PARAM_STR);
$query->bindParam(':Address',$Address,PDO::PARAM_STR);
$query->bindParam(':StudentEmail',$StudentEmail,PDO::PARAM_STR);
$query->bindParam(':Gender',$Gender,PDO::PARAM_STR);
$query->bindParam(':DOB',$DOB,PDO::PARAM_STR);
$query->bindParam(':Status',$Status,PDO::PARAM_STR);
$query->bindParam(':Courseid',$Courseid,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Student Created Successfully";
}
else 
{
$error="Something went wrong. Please try again";
}
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Admin| Add Student </title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style>
            .margin-input-bot{
                margin-top: 5%;
            }
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Add Student</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                
                                        <li class="active">Add Student</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Fill the Student info</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">


<div class="form-group">
<label for="default" class="col-sm-2 control-label">Full Name</label>
<div class="col-sm-10">
<input type="text" name="StudentName" class="form-control" id="fullanme"  required="required" autocomplete="off">
</div>
</div>

<div class="form-group">
<label for="default" class="col-sm-2 control-label">Student ID</label>
<div class="col-sm-10">
<input type="text" name="RollId" class="form-control" id="rollid" required="required" autocomplete="off">
</div>
</div>

<div class="form-group">
<label for="default" class="col-sm-2 control-label">Address</label>
<div class="col-sm-10">
<input type="text" name="Address" class="form-control" id="address" required="required" autocomplete="off">
</div>
</div>

<div class="form-group">
<label for="default" class="col-sm-2 control-label">Email</label>
<div class="col-sm-10">
<input type="email" name="StudentEmail" class="form-control" id="email" required="required" autocomplete="off">
</div>
</div>



<div class="form-group">
<label for="default" class="col-sm-2 control-label">Gender</label>
<div class="col-sm-10">
<input type="radio" name="Gender" value="Male" required="required" checked>Male 
<input type="radio" name="Gender" value="Female" required="required">Female 
<input type="radio" name="Gender" value="Other" required="required">Other
</div>
</div>



<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Select Course</label>
                                                        <div class="col-sm-10">
                                                        <input list="CourseNames" name="Courseid" id="default" class="form-control" placeholder="Course" required>
                                                        <datalist id="CourseNames">
<?php $sql = "SELECT * from tblcourses order by CourseCode";
$query = $dbh->prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<option value="<?php echo htmlentities($result->c_id . '/' .$result->CourseCode); ?>"><?php echo htmlentities($result->CourseCode); ?></option>
<?php }} ?>
</datalist>
                                                        </div>
                                                    </div>

<div class="form-group">
                                                        <label for="date" class="col-sm-2 control-label">DOB</label>
                                                        <div class="col-sm-10">
                <input type="date"  name="DOB" class="form-control" value="<?php echo htmlentities($result->DOB)?>" id="date">
                                                        </div>
                                                    </div>

<div class="form-group">
<label for="default" class="col-sm-2 control-label">Status</label>
<div class="col-sm-10">
<input type="radio" name="Status" value="1" required="required" checked>Active 
<input type="radio" name="Status" value="0" required="required">Block
</div>
</div>           
                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-2">
                                                            <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                                        </div>
                                                        </form>
                                                        <div class="col-sm-4 text-right">
                                                            <u><i class="text-danger" class="margin-input-bot">Or Import Multiple Student Here (.csv)</i></u>
                                                        </div>
                                                        <form method="post" enctype="multipart/form-data">
                                                        <div class="col-sm-2 text-right">
                                                                <input type="file" accept=".csv" name="excel" class="margin-input-bot" />
                                                        </div>
                                                        <div class="col-sm-2 text-center">
                                                            <input type="submit" name="import" class="btn btn-info" value="Import" />
                                                        </div>
                                                        </form>
                                                        <div class="col-sm-12 text-right">
                                                            <i class="text-success" class="margin-input-bot"><?php echo $output; ?></i>
                                                        </div>
                                                    </div>
                                                    <?php if(!isset($_GET['show_instructions'])): ?>
                                                    <div class="row">
                                                        <div class="col-sm-10 col-sm-offset-1 text-right">
                                                            <a href="create-student-try.php?show_instructions=true"><i class="fa fa-plus"></i> Show Instructions on Uploading</a>
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                                    <?php if(isset($_GET['show_instructions'])): ?>
                                                    <div class="row">
                                                        <div class="col-sm-10 col-sm-offset-1 text-right">
                                                            <a href="create-student-try.php"><i class="fa fa-minus"></i> Hide Instructions</a>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-sm-10 col-sm-offset-1 margin-input-bot">
                                                            How to upload excel (.csv) file:
                                                            <br>
                                                            <br>
                                                            1. Create new excel (.csv) file with first row as column header (Student Name,Roll ID, Address, Email, Gender, Date of Birth, Course id) in correct order<br>
                                                            2. Courseid can be found in Courses Menu/ Manage Courses. Search the course then get the id at first column of table presented.<br>
                                                            3. Then supply the corresponding columns with Student info<br>
                                                            4. Save the file as .csv<br>
                                                            5. In Add Student interface click Choose File button then navigate the saved .csv file.<br>
                                                            6. Click Import to add Students. Done.
                                                        </div>
                                                    </div>
                                                    <?php endif; ?>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>
                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>

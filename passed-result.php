<?php
session_start();
error_reporting(0);
include('includes/config.php');
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grade Inquiry System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body>
                    <!-- /.left-sidebar -->

                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-12">
                                    <h2 class="title" align="center">Semestral Passed Students Report <?php if(isset($_POST['proghead'])){echo "(".$_SESSION['DepartmentName'].")";} ?></h2>
                                </div>
                            </div>
                            <!-- /.row -->
                          
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">

                                <div class="row">
                                    <div class="col-md-8 col-md-offset-2">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
<?php
if(isset($_POST)){
$Semester = $_POST['Semester'];
$SchoolYear = $_POST['SchoolYear'];
 ?>
                                            </div>
                                            <div class="panel-body p-20">
                                                <table class="table table-hover table-bordered" style="width:100% !important;">
                                                <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Roll ID</th>
                                                            <th>Student Name</th>
                                                            <th>Section</th>
                                                            <th>Subject Code</th>
                                                            <th>Descriptive Title</th>
                                                            <th>Final Grade</th> 
                                                            <th>Units</th>
                                                            <th>Instructor</th>

                                                        </tr>
                                               </thead>

                                                	<tbody>
<?php                                              
// Code for result
$getCourse = "IFNULL((Select CourseCode from tblcourses i where i.c_id=s.Courseid limit 1), 'N/A') as Course";
$getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
$getSection = "IFNULL((SELECT CONCAT(co.CourseCode,'-',sec.Year,' ',sec.Section) from tblcourses co where sec.Courseid=co.c_id limit 1), 'N/A') as Section";
$getSubjectCode = "IFNULL((Select SubjectCode from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectCode";
$getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
$getGrade = "IFNULL((Select marks from tblresult i where i.AssignStudentid=a.id limit 1), 'N/A') as Mark";
$getRemarks = "IFNULL((Select remarks from tblresult i where i.AssignStudentid=a.id limit 1), 'N/A') as Remarks";
$getUnit = "IFNULL((Select units from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as units";
if(isset($_POST['proghead'])){
    $sql = "SELECT s.*, a.StudentYearLevel, ".$getCourse.", ".$getSubjectCode.", ".$getSubject.", ".$getGrade.", ".$getRemarks.", ".$getUnit.", ".$getInstructor.", ".$getSection." from tblstudents s, tblassignstudent a, tblclasses c, tblsection sec, tblresult res, tblcourses cour where cour.c_id = sec.Courseid and cour.Departmentid=".$_SESSION['Departmentid']." and res.AssignStudentid=a.id and res.remarks='PASSED' and c.Sectionid=sec.id and a.Classid=c.id and s.StudentId=a.Studentid and c.Semester=:Semester and c.SchoolYear=:SchoolYear ORDER by s.StudentName ASC";
}else{
    $sql = "SELECT s.*, a.StudentYearLevel, ".$getCourse.", ".$getSubjectCode.", ".$getSubject.", ".$getGrade.", ".$getRemarks.", ".$getUnit.", ".$getInstructor.", ".$getSection." from tblstudents s, tblassignstudent a, tblclasses c, tblsection sec, tblresult res where res.AssignStudentid=a.id and res.remarks='PASSED' and c.Sectionid=sec.id and a.Classid=c.id and s.StudentId=a.Studentid and c.Semester=:Semester and c.SchoolYear=:SchoolYear ORDER by s.StudentName ASC";
}
$query= $dbh -> prepare($sql);
$query->bindParam(':Semester',$Semester,PDO::PARAM_STR);
$query->bindParam(':SchoolYear',$SchoolYear,PDO::PARAM_STR);
$query-> execute();  
$results = $query -> fetchAll(PDO::FETCH_OBJ);
$cnt=1;



if($countrow=$query->rowCount()>0)
{ 
$total_units = 0;
foreach($results as $result){

    ?>

                         <tr>
                        <th scope="row"><?php echo htmlentities($cnt);?></th>
                        <td><?php echo htmlentities($result->RollId);?></td>
                        <td><?php echo htmlentities($result->StudentName);?></td>
                        <td><?php echo htmlentities($result->Section);?></td>
                        <td><?php echo htmlentities($result->SubjectCode);?></td>
                        <td><?php echo htmlentities($result->SubjectName);?></td>
                        <td class="text-center"><?php echo htmlentities($result->Mark);?></td>
                        <td class="text-center"><?php echo htmlentities($result->units);?></td>            
                        <td><?php echo htmlentities($result->Instructor);?></td>                                      
                        </tr>
<?php 
$total_units+=$result->units;
$cnt++;}
?>

                     <!--<tr>
        <th colspan="7" scope="row" class="text-right" colspan="2">Total Units:</th>           
                    <td class="text-center"><?php echo htmlentities($total_units); ?></td>
                    <td></td>
                     </tr>-->
                    <tr>
        <th colspan="8" scope="row" colspan="2">Download Failed Report</th>           
                    <td><button class="btn btn-primary" onclick="window.print()">Print this page</button></td>
                     </tr>

 <?php } else { ?>     
<div class="alert alert-warning left-icon-alert" role="alert">
                                            <strong>Notice!</strong> No Records Found.
 <?php }
?>
                                        </div>
                                        </div>



                                                	</tbody>
                                                </table>
                                                <div class="form-group">
                                                        <div class="col-sm-offset- col-sm-2">
                                                            <a href="dashboard.php" button type="submit" name="submit" class="btn btn-success"> Back to Dashboard/Home</button>
                                            </div></a>

                                           
                                                    
                                                       

                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->
<?php } ?>

  
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function($)  {
                $('#example').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->

    </body>
</html>

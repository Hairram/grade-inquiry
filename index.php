 <?php
session_start();
// error_reporting(0);
include('includes/config.php');

if(isset($_POST['login']))
{
    if($_POST['loginAs'] == 'Admin'){
        $uname=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT * FROM admin WHERE UserName=:uname and Password=:password";
        $query= $dbh -> prepare($sql);
        $query-> bindParam(':uname', $uname, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            $_SESSION['alogin']='success';
            $_SESSION['UserName']=$_POST['username'];
            foreach ($results as $row) {
                $_SESSION['user_id'] =  $row['id'];
                $_SESSION['AdminFullName'] = $row['AdminFullName'];
            }
            header("Location: dashboard.php");
        } else{

            echo "<script>alert('Invalid Details');</script>";

        }
    }elseif($_POST['loginAs'] == 'Student'){
        $rollid=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT RollId,StudentName,Password FROM tblstudents WHERE RollId=:rollid and Password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':rollid', $rollid, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['studRollid'] =  $row['RollId'];
                $_SESSION['studName'] = $row['StudentName'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'search-result.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }elseif($_POST['loginAs'] == 'Instructor'){
        $rollid=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT InstructorID,InstructorFullName,RollId,Password,Departmentid FROM tblinstructor WHERE RollId=:rollid and Password=:password";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':rollid', $rollid, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['InstructorID'] =  $row['InstructorID'];
                $_SESSION['InstructorFullName'] = $row['InstructorFullName'];
                $_SESSION['Departmentid'] = $row['Departmentid'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-instructor2.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }else{
        $rollid=$_POST['username'];
        $password=$_POST['password'];
        $sql ="SELECT p.*, d.DepartmentName FROM tblproghead p, tbldepartment d WHERE p.RollId=:rollid and p.Password=:password and d.id=p.Departmentid";
        $query= $dbh->prepare($sql);
        $query-> bindParam(':rollid', $rollid, PDO::PARAM_STR);
        $query-> bindParam(':password', $password, PDO::PARAM_STR);
        $query-> execute();
        $results=$query->fetchAll();
        if($query->rowCount() > 0)
        {
            foreach ($results as $row) {
                $_SESSION['ph_id'] =  $row['ph_id'];
                $_SESSION['ph_FullName'] = $row['ProgHeadFullName'];
                $_SESSION['Departmentid'] = $row['Departmentid'];
                $_SESSION['DepartmentName'] = $row['DepartmentName'];
         }
        $_SESSION['alogin']=$_POST['username'];
        echo "<script type='text/javascript'> document.location = 'find-proghead.php'; </script>";
        } else{
        
            echo "<script>alert('Invalid Details');</script>";
        
        }
    }
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grade Inquiry</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
        <style>
            .section-row {
                border-radius: 10px;
            }
        </style>
    </head>
    <body class="">
        <div class="main-wrapper">

            <div class="">
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 text-center">
                        <img src="images/LogoOcc.png" width="200" height="200">
                        <h1 align="center">Opol Community College Grade Inquiry</h1>
                        <br>
                    </div>
        
                                        <div class="col-md-4 col-md-offset-4">
                                            <div class="panel section-row">
                                                <div class="panel-heading">
                                                    <div class="panel-title text-center">
                                                        <h3><i class="fa fa-user"></i> User Login</h3>
                                                    </div>
                                                </div>
                                                <div class="panel-body p-10">
                                                    <form class="form-horizontal" method="post">
                                                        <div class="form-group">
                                                            <label for="loginAs" class="col-sm-3 control-label">Login as:  </label>
                                                    <div class="col-sm-9">
                                                        <select class="form-control" id="loginAs" name="loginAs">
                                                            <option value="Admin">Admin</option>
                                                            <option value="Program Head">Program Head</option>  
                                                            <option value="Instructor">Instructor</option>
                                                            <option value="Student">Student</option>
                                                        </select>
                                                            </div>
                                                        </div>
                                                        <div class="form-group mt-20">
                                                            <label for="username" class="col-sm-3 control-label">Username</label>
                                                            <div class="col-sm-9">
                                                                <input type="text" name="username" class="form-control" id="username" placeholder="User Name">
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword3" class="col-sm-3 control-label">Password</label>
                                                            <div class="col-sm-9">
                                                                <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password">
                                                            </div>
                                                        </div>

                                                        <div class="form-group mt-20">
                                                            <div class="col-sm-offset-2 col-sm-10">
                                                                <button type="submit" name="login" class="btn btn-success btn-labeled pull-right">Sign in<span class="btn-label btn-label-right"><i class="fa fa-check"></i></span></button>
                                                            </div>
                                                        </div>
                                                    </form>

                                                </div>
                                            </div>
                                            <!-- /.panel -->

                                        </div>
                                        <!-- /.col-md-11 -->
                                    </div>

                </div>
                <!-- /.row -->






            </div>
            <!-- /. -->




        </div>
        <!-- /.main-wrapper -->


        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/jquery-ui/jquery-ui.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
            $(function(){

            });
        </script>

        <!-- ========== ADD custom.js FILE BELOW WITH YOUR CHANGES ========== -->
    </body>
</html>

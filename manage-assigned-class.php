
<?php
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
        
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
        <title>OCC Grade Inquiry System</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" type="text/css" href="file:///C:/Users/User/Downloads/fontawesome-free-5.15.1-web/css/all.css">
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" > <!-- USED FOR DEMO HELP - YOU CAN REMOVE IT -->
        <link rel="stylesheet" type="text/css" href="js/DataTables/datatables.min.css"/>
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
          <style>
        .errorWrap {
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #dd3d36;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
.succWrap{
    padding: 10px;
    margin: 0 0 20px 0;
    background: #fff;
    border-left: 4px solid #5cb85c;
    -webkit-box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
    box-shadow: 0 1px 1px 0 rgba(0,0,0,.1);
}
        </style>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
   <?php include('includes/topbar-ins.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">
<?php include('includes/leftbar1.php');?>  

                    <div class="main-page">
                        <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Assigned Class/ Subjects</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
            							<li><a href="find-instructor2.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> View Class/ Subjects</li>
            							<li class="active">Assigned Class/ Subjects</li>
            						</ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <!-- /.container-fluid -->

                        <section class="section">
                            <div class="container-fluid">

                             

                                <div class="row">
                                    <div class="col-md-12">
<?php //var_dump($_SESSION); ?>
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>View Assigned Classes</h5>
                                                </div>
                                            </div>
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>

                                            <div class="panel-body p-20">

                                                <table id="example1" class="display table table-striped table-bordered" cellspacing="0" width="100%">
                                                    <thead>
                                                        <tr>
                                                            <th>No.</th>
                                                            <th>Section</th>
                                                            <th class="text-center">Subject Code</th>
                                                            <th class="text-center">Subject</th>
                                                            <th>Units</th>
                                                            <th>Students</th>
                                                            <th>Action</th>
                                                        </tr>
                                                    </thead>
                                                 
                                                    <tbody>
<?php 
$getInstructor = "IFNULL((Select InstructorFullName from tblinstructor i where i.InstructorID=c.Instructorid limit 1), 'N/A') as Instructor";
$getSection = "IFNULL((SELECT CONCAT(co.CourseCode,'-',i.Year,' ',i.Section) from tblsection i, tblcourses co where i.Courseid=co.c_id and i.id=c.Sectionid limit 1), 'N/A') as Section";
$getSubject = "IFNULL((Select SubjectName from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectName";
$getSubjectC = "IFNULL((Select SubjectCode from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as SubjectCode";
$getUnits = "IFNULL((Select units from tblsubjects i where i.id=c.Subjectid limit 1), 'N/A') as units";
$countStudents = "IFNULL((Select Count(i.id) from tblassignstudent i where i.Classid=c.id), 'N/A') as TotalStudents";
$sql = "SELECT c.*, ".$getInstructor.", ".$getSection.", ".$getSubjectC.", ".$getSubject.", ".$countStudents.", ".$getUnits." from tblclasses c where c.Instructorid=:Instructorid and c.Status=1";
$query = $dbh->prepare($sql);
$query->bindParam(':Instructorid',$_SESSION['InstructorID'],PDO::PARAM_STR);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $result)
{   ?>
<tr>
        <td><?php echo htmlentities($cnt);?></td>
        <td><?php echo htmlentities($result->Section);?></td>
        <td class="text-center"><?php echo htmlentities($result->SubjectCode);?></td>
        <td class="text-center"><?php echo htmlentities($result->SubjectName);?></td>
        <td class="text-center"><?php echo htmlentities($result->units);?></td>
        <td class="text-center"><?php echo htmlentities($result->TotalStudents);?></td>
<td class="text-center">
<a href="manage-student-grades.php?Classid=<?php echo htmlentities($result->id);?>"><input type="button" name="submit" value="Manage Grades"> </a>
</td>
</tr>

<?php 
$total_units+=$result->units;
$cnt++;}
?>

                       <tr>
        <th colspan="4" scope="row" class="text-right" colspan="2">Total Units:</th>           
                    <td class="text-center"><?php echo htmlentities($total_units); ?></td>
                    <td></td>
                     </tr>
                    <tr>


<?php $cnt=$cnt+1;}} ?>
                                                       
                                                    
                                                    </tbody>
                                                </table>

                                         
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-6 -->

                                                               
                                                </div>
                                                <!-- /.col-md-12 -->
                                            </div>
                                        </div>
                                        <!-- /.panel -->
                                    </div>
                                    <!-- /.col-md-6 -->

                                </div>
                                <!-- /.row -->

                            </div>
                            <!-- /.container-fluid -->
                        </section>
                        <!-- /.section -->

                    </div>
                    <!-- /.main-page -->

                    

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->

        </div>
        <!-- /.main-wrapper -->

        <!-- ========== COMMON JS FILES ========== -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>

        <!-- ========== PAGE JS FILES ========== -->
        <script src="js/prism/prism.js"></script>
        <script src="js/DataTables/datatables.min.js"></script>

        <!-- ========== THEME JS ========== -->
        <script src="js/main.js"></script>
        <script>
              
        
       

            $(function($) {
                $('#example1').DataTable({
                    dom: 'Bfrtip',
                    buttons: [
                        'copy', 'csv', 'excel', 'pdf', 'print'
                    ]
                });


                $('#example2').DataTable( {
                    "scrollY":        "300px",
                    "scrollCollapse": true,
                    "paging":         false
                } );

                $('#example3').DataTable();
            });
        </script>
    </body>
</html>



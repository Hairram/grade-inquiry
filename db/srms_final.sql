-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 12, 2021 at 06:25 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `srms_final`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `AdminFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `AdminEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `updationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `UserName`, `Password`, `AdminFullName`, `RollId`, `Address`, `AdminEmail`, `Gender`, `DOB`, `updationDate`) VALUES
(1, 'Luna16', 'qwerty', 'Luna Natblida', '2018-1-00000', 'Opol, Misamis Oriental', 'Luna@gmail.com', 'Female', '1993-08-24', '2020-06-11 12:26:07'),
(3, 'Luna15', 'gwapoko', 'Gomeg Pangon', '2018-1-09090', 'Molugan El Salvador City', 'gomeg@gmail.com', 'Male', '1993-04-06', '2020-12-04 07:38:00'),
(4, 'admin', 'admin123', 'admin admin', '2018-1-99991', 'molugan', 'admin@gmail.com', 'Male', '1993-11-15', '2020-12-04 08:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `tblassignstudent`
--

CREATE TABLE `tblassignstudent` (
  `id` int(11) NOT NULL,
  `Classid` int(11) NOT NULL,
  `Studentid` int(11) NOT NULL,
  `StudentYearLevel` int(1) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblassignstudent`
--

INSERT INTO `tblassignstudent` (`id`, `Classid`, `Studentid`, `StudentYearLevel`, `CreationDate`, `UpdationDate`) VALUES
(14, 1, 32, 1, '2021-01-06 18:12:01', '2021-01-06 18:12:01'),
(15, 1, 1, 1, '2021-01-06 18:12:14', '2021-01-06 18:12:14'),
(16, 3, 32, 1, '2021-01-06 19:29:21', '2021-01-06 19:29:21'),
(17, 4, 1, 1, '2021-01-06 19:29:29', '2021-01-06 19:29:29'),
(18, 5, 33, 1, '2021-01-07 05:21:50', '2021-01-07 05:21:50'),
(19, 5, 32, 1, '2021-01-07 05:22:09', '2021-01-07 05:22:09'),
(20, 5, 1, 1, '2021-01-07 05:22:17', '2021-01-07 05:22:17'),
(21, 6, 1, 1, '2021-01-07 16:33:09', '2021-01-07 16:33:09'),
(23, 7, 1, 4, '2021-01-07 17:06:58', '2021-01-07 17:06:58'),
(24, 8, 1, 1, '2021-01-07 17:52:21', '2021-01-07 17:52:21'),
(25, 8, 32, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(26, 8, 40, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(27, 8, 41, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(28, 8, 42, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(29, 8, 43, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(30, 8, 44, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(31, 8, 45, 1, '2021-01-07 17:55:48', '2021-01-07 17:55:48'),
(32, 9, 1, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(33, 9, 32, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(34, 9, 40, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(35, 9, 41, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(36, 9, 42, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(37, 9, 43, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(38, 9, 44, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(39, 9, 45, 4, '2021-01-07 18:23:19', '2021-01-07 18:23:19'),
(40, 10, 43, 4, '2021-01-08 07:59:57', '2021-01-08 07:59:57'),
(41, 10, 1, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(42, 10, 32, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(43, 10, 40, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(44, 10, 41, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(45, 10, 42, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(46, 10, 44, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(47, 10, 45, 4, '2021-01-08 08:00:18', '2021-01-08 08:00:18'),
(48, 10, 0, 4, '2021-01-12 08:32:59', '2021-01-12 08:32:59'),
(49, 1, 46, 1, '2021-01-12 16:56:10', '2021-01-12 16:56:10');

-- --------------------------------------------------------

--
-- Table structure for table `tblclasses`
--

CREATE TABLE `tblclasses` (
  `id` int(11) NOT NULL,
  `Sectionid` int(11) NOT NULL,
  `Subjectid` int(11) NOT NULL,
  `Instructorid` int(11) NOT NULL,
  `Semester` varchar(20) NOT NULL,
  `SchoolYear` varchar(20) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Status` int(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblclasses`
--

INSERT INTO `tblclasses` (`id`, `Sectionid`, `Subjectid`, `Instructorid`, `Semester`, `SchoolYear`, `CreationDate`, `UpdationDate`, `Status`) VALUES
(1, 1, 33, 1, 'First', '2020-2021', '2021-01-06 07:32:11', '2021-01-09 14:57:12', 0),
(3, 3, 3, 5, 'First', '2020-2021', '2021-01-06 19:28:23', '2021-01-09 14:57:12', 0),
(4, 2, 15, 4, 'First', '2020-2021', '2021-01-06 19:28:53', '2021-01-09 14:57:12', 0),
(5, 3, 1, 5, 'First', '2021-2022', '2021-01-07 05:21:28', '2021-01-07 05:23:36', 1),
(6, 1, 2, 1, 'First', '2020-2021', '2021-01-07 16:32:04', '2021-01-09 14:57:12', 0),
(7, 1, 35, 1, 'Second', '2020-2021', '2021-01-07 17:06:07', '2021-01-09 15:12:32', 1),
(8, 2, 6, 4, 'Second', '2020-2021', '2021-01-07 17:51:28', '2021-01-07 18:23:54', 1),
(9, 5, 3, 0, 'First', '2020-2021', '2021-01-07 18:22:08', '2021-01-09 14:57:12', 0),
(10, 5, 14, 1, 'Second', '2020-2021', '2021-01-08 07:59:01', '2021-01-08 08:04:38', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblcourses`
--

CREATE TABLE `tblcourses` (
  `c_id` int(11) NOT NULL,
  `CourseName` varchar(50) NOT NULL,
  `CourseCode` varchar(10) NOT NULL,
  `Departmentid` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcourses`
--

INSERT INTO `tblcourses` (`c_id`, `CourseName`, `CourseCode`, `Departmentid`, `CreationDate`, `UpdationDate`) VALUES
(1, 'Bachelor of Science in Business Administration', 'BSBA', 2, '2021-01-05 13:43:08', '2021-01-06 19:26:12'),
(2, 'Bachelor of Science in Information Technology', 'BSIT', 1, '2021-01-05 14:24:10', '2021-01-06 15:23:24'),
(3, 'Bachelor of Secondary Education', 'BSED', 3, '2021-01-07 14:42:24', '2021-01-07 14:42:24'),
(4, 'Bachelor of Elementary Education', 'BEED', 4, '2021-01-07 15:17:56', '2021-01-07 15:17:56');

-- --------------------------------------------------------

--
-- Table structure for table `tbldepartment`
--

CREATE TABLE `tbldepartment` (
  `id` int(11) NOT NULL,
  `DepartmentName` varchar(500) NOT NULL,
  `DepartmentCode` varchar(100) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbldepartment`
--

INSERT INTO `tbldepartment` (`id`, `DepartmentName`, `DepartmentCode`, `CreationDate`, `UpdationDate`) VALUES
(1, 'BSIT department', '0001', '2021-01-06 03:29:47', '2021-01-07 14:08:57'),
(2, 'BSBA Department', '0002', '2021-01-06 19:25:55', '2021-01-07 14:09:15'),
(3, 'BSED Department', '003', '2021-01-07 14:40:59', '2021-01-07 14:40:59'),
(4, 'BEED Department', '004', '2021-01-07 14:41:11', '2021-01-07 14:41:11');

-- --------------------------------------------------------

--
-- Table structure for table `tblinstructor`
--

CREATE TABLE `tblinstructor` (
  `InstructorID` int(50) NOT NULL,
  `InstructorFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(5000) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `InstructorEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` date NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `Departmentid` int(11) NOT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL,
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstructor`
--

INSERT INTO `tblinstructor` (`InstructorID`, `InstructorFullName`, `RollId`, `Address`, `Password`, `InstructorEmail`, `Gender`, `DOB`, `RegDate`, `Departmentid`, `UpdationDate`, `Status`) VALUES
(1, 'Prof Gomeg', '2018-1-77777', 'Samsdlas', 'gwapokokaayo', 'gomeg@gwapings.com', 'Male', '1993-08-24', '2021-01-06 16:11:00', 1, NULL, 1),
(3, 'Prof Hairram', '2018-1-88888', 'sadasdas', '123', 'hairram@gwapa.com', 'Female', '1998-04-06', '2021-01-07 01:33:52', 2, NULL, 1),
(4, 'Prof Jiah', '2018-1-99999', '', '12345', 'jiah@gmail.com', 'Female', '2018-11-15', '2021-01-06 15:26:56', 1, NULL, 1),
(5, 'Prof Fesio Nal', '2020-2-0002', 'sadasdas', '123', 'asdasdasdas@sad.cfgs', 'Female', '2021-01-05', '2021-01-12 17:14:04', 2, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblproghead`
--

CREATE TABLE `tblproghead` (
  `ph_id` int(11) NOT NULL,
  `ProgHeadFullName` varchar(255) NOT NULL,
  `RollId` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Address` varchar(5000) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Gender` varchar(20) NOT NULL,
  `DOB` date NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `Departmentid` int(11) NOT NULL,
  `UpdationDate` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE current_timestamp(),
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproghead`
--

INSERT INTO `tblproghead` (`ph_id`, `ProgHeadFullName`, `RollId`, `Password`, `Address`, `Email`, `Gender`, `DOB`, `RegDate`, `Departmentid`, `UpdationDate`, `Status`) VALUES
(1, 'Mr Program Head', '2020-0001-01', '123', 'SampleAddress', 'head@gmail.com', 'Male', '2021-01-07', '2021-01-06 18:15:11', 1, '2021-01-06 23:56:58', 1),
(2, 'Mrs Prog Head 2', '2020-1-00002', '2020-1-00002', 'sample', 'head2@gmail.com', 'Female', '2021-01-28', '2021-01-06 19:30:34', 2, '0000-00-00 00:00:00', 1),
(3, 'Gomeg Mars', '2020-1-30303', '2020-1-30303', 'Mars Earth', 'mars@gmail.com', 'Male', '1993-08-24', '2021-01-07 14:08:08', 1, '0000-00-00 00:00:00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblresult`
--

CREATE TABLE `tblresult` (
  `id` int(11) NOT NULL,
  `AssignStudentid` int(11) DEFAULT NULL,
  `marks` varchar(50) DEFAULT NULL,
  `remarks` varchar(100) NOT NULL,
  `INC` int(1) NOT NULL,
  `PostingDate` timestamp NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresult`
--

INSERT INTO `tblresult` (`id`, `AssignStudentid`, `marks`, `remarks`, `INC`, `PostingDate`, `UpdationDate`) VALUES
(4, 14, '2.1', 'PASSED', 0, '2021-01-07 01:18:22', '2021-01-07 13:51:14'),
(30, 15, '2', 'PASSED', 0, '2021-01-07 17:43:55', '2021-01-07 18:03:44'),
(32, 41, '1.3', 'PASSED', 0, '2021-01-08 08:10:28', '2021-01-12 14:25:57'),
(36, 46, '2.5', 'PASSED', 0, '2021-01-08 08:10:28', '2021-01-08 08:11:21'),
(37, 47, '2.2', 'PASSED', 0, '2021-01-08 08:10:29', '2021-01-08 08:11:32'),
(38, 24, NULL, 'INC', 1, '2021-01-11 14:15:06', '2021-01-11 14:15:54'),
(55, 43, '5', 'FAILED', 0, '2021-01-12 15:54:27', NULL),
(56, 44, '5', 'FAILED', 0, '2021-01-12 15:54:27', NULL),
(57, 45, '5', 'FAILED', 0, '2021-01-12 15:54:27', NULL),
(58, 40, '5', 'FAILED', 0, '2021-01-12 15:54:27', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tblsection`
--

CREATE TABLE `tblsection` (
  `id` int(11) NOT NULL,
  `Section` varchar(50) NOT NULL,
  `Courseid` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblsection`
--

INSERT INTO `tblsection` (`id`, `Section`, `Courseid`, `Year`, `CreationDate`, `UpdationDate`) VALUES
(1, 'A', 2, 1, '2021-01-06 06:30:48', '2021-01-07 06:54:14'),
(2, 'B', 2, 1, '2021-01-06 07:06:08', '2021-01-07 06:54:16'),
(3, 'A', 1, 1, '2021-01-06 19:27:26', '2021-01-07 06:54:20'),
(4, 'B', 1, 1, '2021-01-06 19:27:34', '2021-01-07 06:54:29'),
(5, 'A', 2, 4, '2021-01-07 18:20:51', '2021-01-07 18:20:51');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudents`
--

CREATE TABLE `tblstudents` (
  `StudentId` int(11) NOT NULL,
  `StudentName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `StudentEmail` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `DOB` date NOT NULL,
  `Courseid` int(11) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `Password` varchar(20) NOT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudents`
--

INSERT INTO `tblstudents` (`StudentId`, `StudentName`, `RollId`, `Address`, `StudentEmail`, `Gender`, `DOB`, `Courseid`, `RegDate`, `Password`, `UpdationDate`, `Status`) VALUES
(1, 'Johnrome Pangon', '2018-1-00617', 'Gimaylan City', 'gomeg@gmail.com', 'Male', '1993-08-24', 2, '2021-01-06 13:16:13', '2018-1-00617', NULL, 1),
(33, 'Student Sample1', '2000-1-123', 'sample', 'sample@gmail.com', 'Other', '2021-01-07', 1, '2021-01-07 05:21:02', '2000-1-123', NULL, 1),
(40, 'Angelo Pinque', '2018-1-00001', 'Opol', 'dave@gmail.com', 'Male', '1997-11-08', 2, '2021-01-07 17:53:25', '2018-1-00001', NULL, 1),
(41, 'Jamix Cagatan', '2018-1-00002', 'Opol', 'jamix@gmail.com', 'Male', '1992-11-09', 2, '2021-01-07 17:53:25', '2018-1-00002', NULL, 1),
(42, 'Renz Estalane', '2018-1-00003', 'Opol', 'renz@gmail.com', 'Male', '1994-10-21', 2, '2021-01-07 17:53:25', '2018-1-00003', NULL, 1),
(43, 'Marriah Caudor', '2018-1-00004', 'Molugan', 'marriah@gmail.com', 'Female', '1998-04-06', 2, '2021-01-07 17:53:25', '2018-1-00004', NULL, 1),
(44, 'Danilo Bahian', '2018-1-00005', 'Poblacion', 'danilo@gmail.com', 'Male', '1997-01-11', 2, '2021-01-07 17:53:25', '2018-1-00005', NULL, 1),
(45, 'Robbie Rizza', '2018-1-00006', 'Molugan', 'robriz@gmail.com', 'Female', '0000-00-00', 2, '2021-01-07 17:53:25', '2018-1-00006', NULL, 1),
(46, 'Kurdapyo', '2018-1-00024', 'Molugan', 'kur@gmail.com', 'Male', '1993-08-24', 1, '2021-01-12 16:55:32', '2018-1-00024', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblsubjects`
--

CREATE TABLE `tblsubjects` (
  `id` int(11) NOT NULL,
  `SubjectName` varchar(100) NOT NULL,
  `SubjectCode` varchar(100) NOT NULL,
  `units` int(1) NOT NULL,
  `Creationdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubjects`
--

INSERT INTO `tblsubjects` (`id`, `SubjectName`, `SubjectCode`, `units`, `Creationdate`, `UpdationDate`) VALUES
(1, 'Math', 'GenED8', 4, '2020-10-01 11:14:28', '0000-00-00 00:00:00'),
(2, 'NDM', 'IT 406', 3, '2020-10-01 11:14:31', '0000-00-00 00:00:00'),
(3, 'Capstone Project and Research', 'IT 409', 5, '2020-10-07 04:03:13', '2020-10-07 04:03:13'),
(4, 'SIA 2', 'IT 402', 5, '2020-10-07 10:45:28', '2020-10-07 10:45:28'),
(5, 'SAM', 'IT 403', 5, '2020-10-07 10:45:37', '2020-10-07 10:45:37'),
(6, 'WEB', 'IT 404', 5, '2020-10-07 10:45:54', '2020-10-07 10:45:54'),
(7, 'Capstone 1 Research', 'IT 409', 5, '2020-10-23 16:14:01', '2020-10-23 16:14:01'),
(14, 'OOP', 'IT 204', 5, '2020-12-14 03:19:31', '2020-12-14 03:19:31'),
(15, 'Java', 'IT 203', 5, '2020-12-14 03:19:31', '2020-12-14 03:19:31'),
(19, 'Project Management', 'IT 304', 5, '2020-12-14 03:33:31', '2020-12-14 03:33:31'),
(25, 'SIA', 'IT 407', 5, '2020-12-14 03:33:31', '2020-12-14 03:33:31'),
(33, 'BASIC PROG', 'IT 202', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(34, 'VB6', 'IT 203', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(35, 'PYTHON', 'IT 204', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(36, 'WEBDEV', 'IT 205', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(37, 'SQL', 'IT 206', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(38, 'Physics', 'PHY102', 5, '2021-01-07 17:57:14', '2021-01-07 17:57:14'),
(39, 'English', 'ENG102', 3, '2021-01-07 17:57:14', '2021-01-07 17:57:14'),
(40, 'Mathematics', 'MATH102', 3, '2021-01-07 17:57:14', '2021-01-07 17:57:14'),
(41, 'Physical Fitness', 'P.E.102', 3, '2021-01-07 17:57:14', '2021-01-07 17:57:14');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `RollId` (`RollId`);

--
-- Indexes for table `tblassignstudent`
--
ALTER TABLE `tblassignstudent`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Classid` (`Classid`);

--
-- Indexes for table `tblclasses`
--
ALTER TABLE `tblclasses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcourses`
--
ALTER TABLE `tblcourses`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbldepartment`
--
ALTER TABLE `tbldepartment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  ADD PRIMARY KEY (`InstructorID`);

--
-- Indexes for table `tblproghead`
--
ALTER TABLE `tblproghead`
  ADD PRIMARY KEY (`ph_id`);

--
-- Indexes for table `tblresult`
--
ALTER TABLE `tblresult`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsection`
--
ALTER TABLE `tblsection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblstudents`
--
ALTER TABLE `tblstudents`
  ADD PRIMARY KEY (`StudentId`);

--
-- Indexes for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblassignstudent`
--
ALTER TABLE `tblassignstudent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `tblclasses`
--
ALTER TABLE `tblclasses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `tblcourses`
--
ALTER TABLE `tblcourses`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbldepartment`
--
ALTER TABLE `tbldepartment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  MODIFY `InstructorID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `tblproghead`
--
ALTER TABLE `tblproghead`
  MODIFY `ph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tblresult`
--
ALTER TABLE `tblresult`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=59;

--
-- AUTO_INCREMENT for table `tblsection`
--
ALTER TABLE `tblsection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblstudents`
--
ALTER TABLE `tblstudents`
  MODIFY `StudentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tblassignstudent`
--
ALTER TABLE `tblassignstudent`
  ADD CONSTRAINT `tblassignstudent_ibfk_1` FOREIGN KEY (`Classid`) REFERENCES `tblclasses` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

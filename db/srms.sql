-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 06, 2020 at 04:14 PM
-- Server version: 10.4.14-MariaDB
-- PHP Version: 7.4.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `srms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `AdminFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `AdminEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `updationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `UserName`, `Password`, `AdminFullName`, `RollId`, `Address`, `AdminEmail`, `Gender`, `DOB`, `updationDate`) VALUES
(1, 'Luna16', 'qwerty123', 'Luna Natblida', '2018-1-00000', 'Opol, Misamis Oriental', 'Luna@gmail.com', 'Female', '1993-08-24', '2020-06-11 12:26:07'),
(3, 'Luna15', 'gwapoko', 'Gomeg Pangon', '2018-1-09090', 'Molugan El Salvador City', 'gomeg@gmail.com', 'Male', '1993-24-08', '2020-12-04 07:38:00'),
(4, 'admin', 'admin123', 'admin admin', '2018-1-99991', 'molugan', 'admin@gmail.com', 'Male', '1993-24-08', '2020-12-04 08:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `tbladmin`
--

CREATE TABLE `tbladmin` (
  `AdminId` int(50) NOT NULL,
  `id` int(11) NOT NULL,
  `AdminName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `AdminEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL,
  `status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbladmin`
--

INSERT INTO `tbladmin` (`AdminId`, `id`, `AdminName`, `RollId`, `Password`, `Address`, `AdminEmail`, `Gender`, `DOB`, `RegDate`, `UpdationDate`, `status`) VALUES
(1, 1, 'Admin Luna', '2018-1-00000', 'Gwapo', 'city of light', 'trikru@gmail.com', 'Female', '11-11-1111', '2020-10-20 11:08:59', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblclasses`
--

CREATE TABLE `tblclasses` (
  `id` int(11) NOT NULL,
  `ClassName` varchar(80) DEFAULT NULL,
  `InstructorID` int(11) NOT NULL,
  `Semester` varchar(80) NOT NULL,
  `Section` varchar(5) NOT NULL,
  `School_Year` varchar(50) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblclasses`
--

INSERT INTO `tblclasses` (`id`, `ClassName`, `InstructorID`, `Semester`, `Section`, `School_Year`, `CreationDate`, `UpdationDate`) VALUES
(1, 'BSBA 1', 1, 'Second', 'A', '2020-2021', '2020-10-01 11:13:55', '0000-00-00 00:00:00'),
(2, 'BSIT 4', 1, 'Second', 'B', '2020-2021', '2020-10-01 11:14:19', '2020-10-07 03:18:59'),
(3, 'BSBA', 3, 'First', 'A', '2020-2021', '2020-10-01 11:27:23', '0000-00-00 00:00:00'),
(15, 'BSED 4', 3, 'First', 'C', '2020-2021', '2020-10-13 06:32:14', '2020-10-13 06:32:14'),
(16, 'BEED 4', 3, 'Second', 'B', '2020-2021', '2020-10-13 06:39:15', '2020-10-13 06:39:15');

-- --------------------------------------------------------

--
-- Table structure for table `tblclasses_instructor`
--

CREATE TABLE `tblclasses_instructor` (
  `id` int(11) NOT NULL,
  `ClassID` int(11) NOT NULL,
  `InstructorID` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblclasses_instructor`
--

INSERT INTO `tblclasses_instructor` (`id`, `ClassID`, `InstructorID`) VALUES
(1, 2, 4),
(2, 16, 3);

-- --------------------------------------------------------

--
-- Table structure for table `tblinstructor`
--

CREATE TABLE `tblinstructor` (
  `InstructorID` int(50) NOT NULL,
  `InstructorFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `InstructorEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL,
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstructor`
--

INSERT INTO `tblinstructor` (`InstructorID`, `InstructorFullName`, `RollId`, `Password`, `InstructorEmail`, `Gender`, `DOB`, `RegDate`, `UpdationDate`, `Status`) VALUES
(1, 'Prof Gomeg', '2018-1-77777', 'gwapokokaayo', 'gomeg@gwapings.com', 'Male', '1993-08-24', '2020-12-05 06:45:31', NULL, 1),
(3, 'Prof Hairram', '2018-1-88888', 'gwapako', 'hairram@gwapa.com', 'Female', '1998-04-06', '2020-10-08 17:01:26', NULL, 1),
(4, 'Prof Jiah', '2018-1-99999', '12345', 'jiah@gmail.com', 'Female', '2018-11-15', '2020-10-08 17:31:30', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblresult`
--

CREATE TABLE `tblresult` (
  `id` int(11) NOT NULL,
  `StudentId` int(11) DEFAULT NULL,
  `ClassId` int(11) DEFAULT NULL,
  `SubjectId` int(11) DEFAULT NULL,
  `marks` int(11) DEFAULT NULL,
  `remarks` varchar(100) NOT NULL,
  `ReExam` varchar(50) NOT NULL,
  `PostingDate` timestamp NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresult`
--

INSERT INTO `tblresult` (`id`, `StudentId`, `ClassId`, `SubjectId`, `marks`, `remarks`, `ReExam`, `PostingDate`, `UpdationDate`) VALUES
(1, 1, 1, 1, 90, 'PASSED', 'no', '2020-10-01 11:18:48', '2020-10-07 11:23:54'),
(2, 5, 2, 3, 90, 'PASSED', 'no', '2020-10-07 04:12:27', '2020-10-07 11:23:55'),
(3, 5, 2, 2, 90, 'PASSED', 'no', '2020-10-07 04:12:27', '2020-10-07 11:23:57'),
(4, 4, 2, 3, 90, 'PASSED', 'no', '2020-10-07 05:03:03', '2020-10-07 11:23:58'),
(5, 4, 2, 2, 90, 'PASSED', 'no', '2020-10-07 05:03:03', '2020-10-07 11:24:00'),
(6, 2, 2, 3, 90, '', '', '2020-10-07 15:00:23', NULL),
(7, 2, 2, 2, 90, '', '', '2020-10-07 15:00:24', NULL),
(8, 2, 2, 5, 90, '', '', '2020-10-07 15:00:24', NULL),
(9, 2, 2, 4, 90, '', '', '2020-10-07 15:00:24', NULL),
(10, 2, 2, 6, 90, '', '', '2020-10-07 15:00:24', NULL),
(11, 2, 2, 3, 90, '', '', '2020-10-07 15:15:58', NULL),
(12, 2, 2, 2, 90, '', '', '2020-10-07 15:15:58', NULL),
(13, 2, 2, 5, 90, '', '', '2020-10-07 15:15:58', NULL),
(14, 2, 2, 4, 90, '', '', '2020-10-07 15:15:58', NULL),
(15, 2, 2, 6, 90, '', '', '2020-10-07 15:15:58', NULL),
(16, 2, 2, 3, 90, '', '', '2020-10-07 15:16:51', NULL),
(17, 2, 2, 2, 90, '', '', '2020-10-07 15:16:51', NULL),
(18, 2, 2, 5, 90, '', '', '2020-10-07 15:16:51', NULL),
(19, 2, 2, 4, 90, '', '', '2020-10-07 15:16:51', NULL),
(20, 2, 2, 6, 90, '', '', '2020-10-07 15:16:51', NULL),
(21, 6, 2, 3, 80, '', 'no', '2020-10-11 16:37:26', '2020-12-06 15:00:04'),
(22, 6, 2, 2, 80, '', 'no', '2020-10-11 16:37:26', '2020-12-06 15:00:05'),
(23, 6, 2, 5, 80, '', 'no', '2020-10-11 16:37:26', '2020-12-06 15:00:06'),
(24, 6, 2, 4, 80, '', 'no', '2020-10-11 16:37:26', '2020-12-06 15:00:07'),
(25, 6, 2, 6, 80, '', 'no', '2020-10-11 16:37:26', '2020-12-06 15:00:10'),
(26, 13, 18, 8, 80, '', 'no', '2020-10-23 16:58:49', '2020-12-06 15:00:12');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudents`
--

CREATE TABLE `tblstudents` (
  `StudentId` int(11) NOT NULL,
  `StudentName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `StudentEmail` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `ClassId` int(11) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudents`
--

INSERT INTO `tblstudents` (`StudentId`, `StudentName`, `RollId`, `Address`, `StudentEmail`, `Gender`, `DOB`, `ClassId`, `RegDate`, `UpdationDate`, `Status`) VALUES
(4, 'Marriah Arlle Jane Caudor Pangon', '2018-1-01059', 'mOLUGAN', 'marriah@gwapa.com', 'Female', '1998-04-06', 2, '2020-10-06 16:31:50', '2020-10-19 18:14:41', 1),
(5, 'Johnrome Pangon', '2018-1-00617', 'Gimaylan City', 'gomeg@gmail.com', 'Male', '1993-08-24', 2, '2020-10-07 03:17:40', '2020-10-19 18:02:36', 1),
(6, 'Danilo Bahian', '2018-1-00529', 'psb', 'danilo@gmail.com', 'Male', '1997-01-11', 2, '2020-10-09 05:58:22', '2020-10-19 18:14:46', 1),
(13, 'Johnrome Pangon II', '2018-1-00616', 'Molugan', 'gomeg@yahoo.com', 'Male', '1993-08-24', 18, '2020-10-23 16:58:09', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblsubjectcombination`
--

CREATE TABLE `tblsubjectcombination` (
  `id` int(11) NOT NULL,
  `ClassId` int(11) NOT NULL,
  `SubjectId` int(11) NOT NULL,
  `status` int(1) DEFAULT NULL,
  `InstructorID` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `Updationdate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubjectcombination`
--

INSERT INTO `tblsubjectcombination` (`id`, `ClassId`, `SubjectId`, `status`, `InstructorID`, `CreationDate`, `Updationdate`) VALUES
(1, 1, 1, 1, 0, '2020-10-01 11:14:54', '2020-10-01 11:14:54'),
(2, 2, 2, 1, 0, '2020-10-01 11:14:58', '2020-10-01 11:14:58'),
(3, 2, 3, 1, 0, '2020-10-07 04:11:05', '2020-10-07 04:11:05'),
(6, 2, 4, 1, 0, '2020-10-07 11:05:12', '2020-10-07 11:05:12'),
(7, 2, 5, 1, 0, '2020-10-07 11:05:17', '2020-10-07 11:05:17'),
(9, 2, 6, 1, 0, '2020-10-07 11:05:27', '2020-10-07 11:05:27'),
(10, 17, 7, 1, 0, '2020-10-23 16:14:54', '2020-10-23 16:14:54'),
(11, 18, 8, 1, 0, '2020-10-23 16:51:24', '2020-10-23 16:51:24'),
(12, 19, 9, 0, 0, '2020-10-24 11:18:01', '2020-10-24 11:18:01'),
(16, 16, 2, 1, 0, '2020-12-05 05:51:36', '2020-12-05 05:51:36'),
(39, 2, 1, 1, 3, '2020-12-05 06:14:04', '2020-12-05 06:14:04');

-- --------------------------------------------------------

--
-- Table structure for table `tblsubjects`
--

CREATE TABLE `tblsubjects` (
  `id` int(11) NOT NULL,
  `SubjectName` varchar(100) NOT NULL,
  `SubjectCode` varchar(100) NOT NULL,
  `units` int(1) NOT NULL,
  `Creationdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubjects`
--

INSERT INTO `tblsubjects` (`id`, `SubjectName`, `SubjectCode`, `units`, `Creationdate`, `UpdationDate`) VALUES
(1, 'Math', 'GenED8', 3, '2020-10-01 11:14:28', '0000-00-00 00:00:00'),
(2, 'NDM', 'IT 406', 3, '2020-10-01 11:14:31', '0000-00-00 00:00:00'),
(3, 'Capstone Project and Research', 'IT 409', 5, '2020-10-07 04:03:13', '2020-10-07 04:03:13'),
(4, 'SIA 2', 'IT 402', 5, '2020-10-07 10:45:28', '2020-10-07 10:45:28'),
(5, 'SAM', 'IT 403', 5, '2020-10-07 10:45:37', '2020-10-07 10:45:37'),
(6, 'WEB', 'IT 404', 5, '2020-10-07 10:45:54', '2020-10-07 10:45:54'),
(7, 'Capstone 1 Research', 'IT 409', 5, '2020-10-23 16:14:01', '2020-10-23 16:14:01'),
(8, 'Capstone Project and Research', 'IT 409', 5, '2020-10-23 16:50:20', '2020-10-23 16:50:20'),
(9, 'Capstone Project and Research', 'IT 409', 5, '2020-10-24 11:17:33', '2020-10-24 11:17:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `RollId` (`RollId`);

--
-- Indexes for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD PRIMARY KEY (`AdminId`),
  ADD KEY `id` (`id`);

--
-- Indexes for table `tblclasses`
--
ALTER TABLE `tblclasses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblclasses_instructor`
--
ALTER TABLE `tblclasses_instructor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ClassID` (`ClassID`),
  ADD KEY `InstructorID` (`InstructorID`);

--
-- Indexes for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  ADD PRIMARY KEY (`InstructorID`);

--
-- Indexes for table `tblresult`
--
ALTER TABLE `tblresult`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblstudents`
--
ALTER TABLE `tblstudents`
  ADD PRIMARY KEY (`StudentId`);

--
-- Indexes for table `tblsubjectcombination`
--
ALTER TABLE `tblsubjectcombination`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblclasses`
--
ALTER TABLE `tblclasses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `tblclasses_instructor`
--
ALTER TABLE `tblclasses_instructor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  MODIFY `InstructorID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblresult`
--
ALTER TABLE `tblresult`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `tblstudents`
--
ALTER TABLE `tblstudents`
  MODIFY `StudentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `tblsubjectcombination`
--
ALTER TABLE `tblsubjectcombination`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbladmin`
--
ALTER TABLE `tbladmin`
  ADD CONSTRAINT `tbladmin_ibfk_1` FOREIGN KEY (`id`) REFERENCES `admin` (`id`);

--
-- Constraints for table `tblclasses_instructor`
--
ALTER TABLE `tblclasses_instructor`
  ADD CONSTRAINT `tblclasses_instructor_ibfk_1` FOREIGN KEY (`ClassID`) REFERENCES `tblclasses` (`id`),
  ADD CONSTRAINT `tblclasses_instructor_ibfk_2` FOREIGN KEY (`InstructorID`) REFERENCES `tblinstructor` (`InstructorID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 06, 2021 at 09:51 AM
-- Server version: 10.4.10-MariaDB
-- PHP Version: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `srms`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `UserName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `AdminFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `AdminEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `updationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `UserName`, `Password`, `AdminFullName`, `RollId`, `Address`, `AdminEmail`, `Gender`, `DOB`, `updationDate`) VALUES
(1, 'Luna16', 'qwerty123', 'Luna Natblida', '2018-1-00000', 'Opol, Misamis Oriental', 'Luna@gmail.com', 'Female', '1993-08-24', '2020-06-11 12:26:07'),
(3, 'Luna15', 'gwapoko', 'Gomeg Pangon', '2018-1-09090', 'Molugan El Salvador City', 'gomeg@gmail.com', 'Male', '1993-04-06', '2020-12-04 07:38:00'),
(4, 'admin', 'admin123', 'admin admin', '2018-1-99991', 'molugan', 'admin@gmail.com', 'Male', '1993-11-15', '2020-12-04 08:39:02');

-- --------------------------------------------------------

--
-- Table structure for table `tblassignstudent`
--

CREATE TABLE `tblassignstudent` (
  `id` int(11) NOT NULL,
  `Classid` int(11) NOT NULL,
  `Studentid` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `tblclasses`
--

CREATE TABLE `tblclasses` (
  `id` int(11) NOT NULL,
  `Sectionid` int(11) NOT NULL,
  `Subjectid` int(11) NOT NULL,
  `Instructorid` int(11) NOT NULL,
  `Semester` varchar(20) NOT NULL,
  `SchoolYear` varchar(20) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblclasses`
--

INSERT INTO `tblclasses` (`id`, `Sectionid`, `Subjectid`, `Instructorid`, `Semester`, `SchoolYear`, `CreationDate`, `UpdationDate`) VALUES
(1, 1, 33, 0, 'First', '2020-2021', '2021-01-06 07:32:11', '2021-01-06 07:32:11'),
(2, 2, 33, 0, 'First', '2020-2021', '2021-01-06 07:32:31', '2021-01-06 07:32:31');

-- --------------------------------------------------------

--
-- Table structure for table `tblcourses`
--

CREATE TABLE `tblcourses` (
  `c_id` int(11) NOT NULL,
  `CourseName` varchar(50) NOT NULL,
  `CourseCode` varchar(10) NOT NULL,
  `Departmentid` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblcourses`
--

INSERT INTO `tblcourses` (`c_id`, `CourseName`, `CourseCode`, `Departmentid`, `CreationDate`, `UpdationDate`) VALUES
(1, 'Bachelor of Science in Business Administration', 'BSBA', 0, '2021-01-05 13:43:08', '2021-01-06 03:47:19'),
(2, 'Bachelor of Science in Information Technology', 'BSIT', 0, '2021-01-05 14:24:10', '2021-01-06 03:48:22');

-- --------------------------------------------------------

--
-- Table structure for table `tbldepartment`
--

CREATE TABLE `tbldepartment` (
  `id` int(11) NOT NULL,
  `DepartmentName` varchar(500) NOT NULL,
  `DepartmentCode` varchar(100) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbldepartment`
--

INSERT INTO `tbldepartment` (`id`, `DepartmentName`, `DepartmentCode`, `CreationDate`, `UpdationDate`) VALUES
(1, 'Department 1', '0001', '2021-01-06 03:29:47', '2021-01-06 03:57:18');

-- --------------------------------------------------------

--
-- Table structure for table `tblinstructor`
--

CREATE TABLE `tblinstructor` (
  `InstructorID` int(50) NOT NULL,
  `InstructorFullName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `InstructorEmail` varchar(100) NOT NULL,
  `Gender` varchar(100) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL,
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblinstructor`
--

INSERT INTO `tblinstructor` (`InstructorID`, `InstructorFullName`, `RollId`, `Password`, `InstructorEmail`, `Gender`, `DOB`, `RegDate`, `UpdationDate`, `Status`) VALUES
(1, 'Prof Gomeg', '2018-1-77777', 'gwapokokaayo', 'gomeg@gwapings.com', 'Male', '1993-08-24', '2020-12-05 06:45:31', NULL, 1),
(3, 'Prof Hairram', '2018-1-88888', 'gwapako', 'hairram@gwapa.com', 'Female', '1998-04-06', '2020-10-08 17:01:26', NULL, 1),
(4, 'Prof Jiah', '2018-1-99999', '12345', 'jiah@gmail.com', 'Female', '2018-11-15', '2020-10-08 17:31:30', NULL, 1),
(5, 'sadsd', 'asdasd', 'dasd', 'asdasdasdas@sad.cfgs', 'Other', '2021-01-05', '2021-01-05 12:25:31', NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tblproghead`
--

CREATE TABLE `tblproghead` (
  `ph_id` int(11) NOT NULL,
  `ph_FullName` varchar(255) NOT NULL,
  `ph_RollId` varchar(255) NOT NULL,
  `ph_Password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblproghead`
--

INSERT INTO `tblproghead` (`ph_id`, `ph_FullName`, `ph_RollId`, `ph_Password`) VALUES
(1, 'Magnus Karlsen', '2020-1-00000', 'master123'),
(2, 'Wesley So', '2020-1-99999', 'Master'),
(3, 'Walter White', '2020-1-88888', 'Legendary');

-- --------------------------------------------------------

--
-- Table structure for table `tblresult`
--

CREATE TABLE `tblresult` (
  `id` int(11) NOT NULL,
  `StudentId` int(11) DEFAULT NULL,
  `ClassId` int(11) DEFAULT NULL,
  `SubjectId` int(11) DEFAULT NULL,
  `marks` varchar(50) DEFAULT NULL,
  `remarks` varchar(100) NOT NULL,
  `ReExam` varchar(50) NOT NULL,
  `PostingDate` timestamp NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblresult`
--

INSERT INTO `tblresult` (`id`, `StudentId`, `ClassId`, `SubjectId`, `marks`, `remarks`, `ReExam`, `PostingDate`, `UpdationDate`) VALUES
(1, 1, 1, 1, '90', 'PASSED', 'no', '2020-10-01 11:18:48', '2020-10-07 11:23:54'),
(2, 5, 2, 3, '90', 'PASSED', 'no', '2020-10-07 04:12:27', '2020-10-07 11:23:55'),
(3, 5, 2, 2, '90', 'PASSED', 'no', '2020-10-07 04:12:27', '2020-10-07 11:23:57'),
(4, 4, 2, 3, '90', 'PASSED', 'no', '2020-10-07 05:03:03', '2020-10-07 11:23:58'),
(5, 4, 2, 2, '90', 'PASSED', 'no', '2020-10-07 05:03:03', '2020-10-07 11:24:00'),
(6, 2, 2, 3, '90', '', '', '2020-10-07 15:00:23', NULL),
(7, 2, 2, 2, '90', '', '', '2020-10-07 15:00:24', NULL),
(8, 2, 2, 5, '90', '', '', '2020-10-07 15:00:24', NULL),
(9, 2, 2, 4, '90', '', '', '2020-10-07 15:00:24', NULL),
(10, 2, 2, 6, '90', '', '', '2020-10-07 15:00:24', NULL),
(11, 2, 2, 3, '90', '', '', '2020-10-07 15:15:58', NULL),
(12, 2, 2, 2, '90', '', '', '2020-10-07 15:15:58', NULL),
(13, 2, 2, 5, '90', '', '', '2020-10-07 15:15:58', NULL),
(14, 2, 2, 4, '90', '', '', '2020-10-07 15:15:58', NULL),
(15, 2, 2, 6, '90', '', '', '2020-10-07 15:15:58', NULL),
(16, 2, 2, 3, '90', '', '', '2020-10-07 15:16:51', NULL),
(17, 2, 2, 2, '90', '', '', '2020-10-07 15:16:51', NULL),
(18, 2, 2, 5, '90', '', '', '2020-10-07 15:16:51', NULL),
(19, 2, 2, 4, '90', '', '', '2020-10-07 15:16:51', NULL),
(20, 2, 2, 6, '90', '', '', '2020-10-07 15:16:51', NULL),
(21, 6, 2, 3, 'INC', 'INC', '', '2020-10-11 16:37:26', '2021-01-03 02:57:08'),
(22, 6, 2, 2, 'INC', 'INC', '', '2020-10-11 16:37:26', '2021-01-03 02:57:27'),
(23, 6, 2, 5, 'INC', 'INC', '', '2020-10-11 16:37:26', '2021-01-03 02:57:30'),
(24, 6, 2, 4, 'INC', 'INC', '', '2020-10-11 16:37:26', '2021-01-03 02:57:33'),
(25, 6, 2, 6, 'INC', 'INC', '', '2020-10-11 16:37:26', '2021-01-03 02:57:35'),
(26, 13, 18, 8, 'INC', 'INC', '', '2020-10-23 16:58:49', '2021-01-03 02:57:37'),
(27, 26, 2, 3, '1', '', '', '2021-01-03 06:29:22', NULL),
(28, 26, 2, 13, '1', '', '', '2021-01-03 06:29:22', NULL),
(29, 26, 2, 1, '1', '', '', '2021-01-03 06:29:22', NULL),
(30, 26, 2, 2, '1', '', '', '2021-01-03 06:29:22', NULL),
(31, 26, 2, 16, '1', '', '', '2021-01-03 06:29:22', NULL),
(32, 26, 2, 5, '1', '', '', '2021-01-03 06:29:22', NULL),
(33, 26, 2, 4, '1', '', '', '2021-01-03 06:29:23', NULL),
(34, 26, 2, 6, '1', '', '', '2021-01-03 06:29:23', NULL),
(35, 24, 2, 19, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:01'),
(36, 24, 2, 14, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:00'),
(37, 24, 2, 7, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:01'),
(38, 25, 2, 15, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:02'),
(39, 25, 2, 19, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:03'),
(40, 25, 2, 14, '3', 'PASSED', '', '2021-01-03 18:20:28', '2021-01-03 18:24:04'),
(41, 25, 2, 7, '3', 'PASSED', '', '2021-01-03 18:20:29', '2021-01-03 18:24:05');

-- --------------------------------------------------------

--
-- Table structure for table `tblsection`
--

CREATE TABLE `tblsection` (
  `id` int(11) NOT NULL,
  `Section` varchar(50) NOT NULL,
  `Courseid` int(11) NOT NULL,
  `Year` int(11) NOT NULL,
  `CreationDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tblsection`
--

INSERT INTO `tblsection` (`id`, `Section`, `Courseid`, `Year`, `CreationDate`, `UpdationDate`) VALUES
(1, 'R1', 2, 1, '2021-01-06 06:30:48', '2021-01-06 06:38:59'),
(2, 'R2', 2, 1, '2021-01-06 07:06:08', '2021-01-06 07:06:08');

-- --------------------------------------------------------

--
-- Table structure for table `tblstudents`
--

CREATE TABLE `tblstudents` (
  `StudentId` int(11) NOT NULL,
  `StudentName` varchar(100) NOT NULL,
  `RollId` varchar(100) NOT NULL,
  `Address` varchar(100) NOT NULL,
  `StudentEmail` varchar(100) NOT NULL,
  `Gender` varchar(10) NOT NULL,
  `DOB` varchar(100) NOT NULL,
  `ClassId` int(11) NOT NULL,
  `RegDate` timestamp NOT NULL DEFAULT current_timestamp(),
  `Password` varchar(20) NOT NULL,
  `UpdationDate` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `Status` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblstudents`
--

INSERT INTO `tblstudents` (`StudentId`, `StudentName`, `RollId`, `Address`, `StudentEmail`, `Gender`, `DOB`, `ClassId`, `RegDate`, `Password`, `UpdationDate`, `Status`) VALUES
(4, 'Marriah Arlle Jane Caudor Pangon', '2018-1-01059', 'mOLUGAN', 'marriah@gwapa.com', 'Female', '1998-04-06', 2, '2020-10-06 16:31:50', '', '2020-10-19 18:14:41', 1),
(5, 'Johnrome Pangon', '2018-1-00617', 'Gimaylan City', 'gomeg@gmail.com', 'Male', '1993-08-24', 2, '2020-10-07 03:17:40', '', '2020-10-19 18:02:36', 1),
(6, 'Danilo Bahian', '2018-1-00529', 'psb', 'danilo@gmail.com', 'Male', '1997-01-11', 2, '2020-10-09 05:58:22', '', '2020-10-19 18:14:46', 1),
(24, 'Jiah Saretha Pangon', '2018-1-00016', '', '', '', '', 2, '2020-12-16 16:58:03', '', '2021-01-03 18:21:57', 1),
(25, 'James Bond', '2018-1-00017', '', '', '', '', 2, '2020-12-16 16:58:03', '', '2021-01-03 18:21:56', 1),
(26, 'Peter Parker', '2018-1-00018', 'molugan', 'peter@gamil.com', '', '', 2, '2020-12-16 16:58:03', '', '2021-01-01 15:49:19', 1),
(27, 'Harley Quin', '2018-1-00002', 'Gotham', 'gotham@gmail.com', 'Female', '1998-05-05', 2, '2021-01-04 16:46:38', '', '2021-01-04 16:54:22', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tblsubjects`
--

CREATE TABLE `tblsubjects` (
  `id` int(11) NOT NULL,
  `SubjectName` varchar(100) NOT NULL,
  `SubjectCode` varchar(100) NOT NULL,
  `units` int(1) NOT NULL,
  `Creationdate` timestamp NOT NULL DEFAULT current_timestamp(),
  `UpdationDate` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tblsubjects`
--

INSERT INTO `tblsubjects` (`id`, `SubjectName`, `SubjectCode`, `units`, `Creationdate`, `UpdationDate`) VALUES
(1, 'Math', 'GenED8', 4, '2020-10-01 11:14:28', '0000-00-00 00:00:00'),
(2, 'NDM', 'IT 406', 3, '2020-10-01 11:14:31', '0000-00-00 00:00:00'),
(3, 'Capstone Project and Research', 'IT 409', 5, '2020-10-07 04:03:13', '2020-10-07 04:03:13'),
(4, 'SIA 2', 'IT 402', 5, '2020-10-07 10:45:28', '2020-10-07 10:45:28'),
(5, 'SAM', 'IT 403', 5, '2020-10-07 10:45:37', '2020-10-07 10:45:37'),
(6, 'WEB', 'IT 404', 5, '2020-10-07 10:45:54', '2020-10-07 10:45:54'),
(7, 'Capstone 1 Research', 'IT 409', 5, '2020-10-23 16:14:01', '2020-10-23 16:14:01'),
(14, 'OOP', 'IT 204', 5, '2020-12-14 03:19:31', '2020-12-14 03:19:31'),
(15, 'Java', 'IT 203', 5, '2020-12-14 03:19:31', '2020-12-14 03:19:31'),
(19, 'Project Management', 'IT 304', 5, '2020-12-14 03:33:31', '2020-12-14 03:33:31'),
(25, 'SIA', 'IT 407', 5, '2020-12-14 03:33:31', '2020-12-14 03:33:31'),
(33, 'BASIC PROG', 'IT 202', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(34, 'VB6', 'IT 203', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(35, 'PYTHON', 'IT 204', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(36, 'WEBDEV', 'IT 205', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05'),
(37, 'SQL', 'IT 206', 3, '2021-01-04 17:08:05', '2021-01-04 17:08:05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `RollId` (`RollId`);

--
-- Indexes for table `tblassignstudent`
--
ALTER TABLE `tblassignstudent`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblclasses`
--
ALTER TABLE `tblclasses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblcourses`
--
ALTER TABLE `tblcourses`
  ADD PRIMARY KEY (`c_id`);

--
-- Indexes for table `tbldepartment`
--
ALTER TABLE `tbldepartment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  ADD PRIMARY KEY (`InstructorID`);

--
-- Indexes for table `tblproghead`
--
ALTER TABLE `tblproghead`
  ADD PRIMARY KEY (`ph_id`),
  ADD UNIQUE KEY `ph_RollId` (`ph_RollId`);

--
-- Indexes for table `tblresult`
--
ALTER TABLE `tblresult`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblsection`
--
ALTER TABLE `tblsection`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tblstudents`
--
ALTER TABLE `tblstudents`
  ADD PRIMARY KEY (`StudentId`);

--
-- Indexes for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tblassignstudent`
--
ALTER TABLE `tblassignstudent`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tblclasses`
--
ALTER TABLE `tblclasses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblcourses`
--
ALTER TABLE `tblcourses`
  MODIFY `c_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbldepartment`
--
ALTER TABLE `tbldepartment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tblinstructor`
--
ALTER TABLE `tblinstructor`
  MODIFY `InstructorID` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `tblproghead`
--
ALTER TABLE `tblproghead`
  MODIFY `ph_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tblresult`
--
ALTER TABLE `tblresult`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- AUTO_INCREMENT for table `tblsection`
--
ALTER TABLE `tblsection`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tblstudents`
--
ALTER TABLE `tblstudents`
  MODIFY `StudentId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tblsubjects`
--
ALTER TABLE `tblsubjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

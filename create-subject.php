<?php
$connect = mysqli_connect("localhost", "root", "", "srms_final");
$output = '';
if(isset($_POST["import"]))
{
    // $extension = end(explode(".", $_FILES["excel"]["name"])); // For getting Extension of selected file
    $value = explode(".", $_FILES["excel"]["name"]);
    $extension = strtolower(array_pop($value));
    $allowed_extension = array("csv"); //allowed extension
    if(in_array($extension, $allowed_extension)) //check selected file extension is present in allowed extension array
    {
    $file = $_FILES["excel"]["tmp_name"]; // getting temporary source of excel file
    include("../OccGradeInquiry /Classes/PHPExcel/IOFactory.php"); // Add PHPExcel Library in this code

    $objPHPExcel = PHPExcel_IOFactory::load($file); // create object of PHPExcel library by using load() method and in load method define path of selected file

    foreach ($objPHPExcel->getWorksheetIterator() as $worksheet)
    {
    $highestRow = $worksheet->getHighestRow();
    for($row=2; $row<=$highestRow; $row++)
    {
    $a = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(0, $row)->getValue());
    $b = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(1, $row)->getValue());
    $c = mysqli_real_escape_string($connect, $worksheet->getCellByColumnAndRow(2, $row)->getValue());
    $query = "Select * FROM tblsubjects where SubjectCode='".$b."'";
    $result = mysqli_num_rows(mysqli_query($connect, $query));
    if($result == 0){
        $query = "INSERT INTO tblsubjects(SubjectName,SubjectCode, units) VALUES ('".$a."', '".$b."', '".$c."')";
        mysqli_query($connect, $query);
    }
    }
    }
    $output .= "<label class='text-success'>Imported Successfully!</label>";
    }
    else
    {
    $output = '<label class="text-danger">Invalid File</label>'; //if non excel file then
    }
}
session_start();
error_reporting(0);
include('includes/config.php');
if(strlen($_SESSION['alogin'])=="")
    {   
    header("Location: index.php"); 
    }
    else{
if(isset($_POST['submit']))
{
$subjectname=$_POST['subjectname'];
$subjectcode=$_POST['subjectcode']; 
$units=$_POST['units'];
$sql="INSERT INTO  tblsubjects(SubjectName,subjectcode,units) VALUES(:subjectname,:subjectcode,:units)";
$query = $dbh->prepare($sql);
$query->bindParam(':subjectname',$subjectname,PDO::PARAM_STR);
$query->bindParam(':subjectcode',$subjectcode,PDO::PARAM_STR);
$query->bindParam(':units',$units,PDO::PARAM_STR);
$query->execute();
$lastInsertId = $dbh->lastInsertId();
if($lastInsertId)
{
$msg="Subject Created successfully";
}
else 
{
$error="Something went wrong. Please try again";
}

}
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    	<meta name="viewport" content="width=device-width, initial-scale=1">
      
        <link rel="stylesheet" href="css/bootstrap.min.css" media="screen" >
        <link rel="stylesheet" href="css/font-awesome.min.css" media="screen" >
        <link rel="stylesheet" href="css/animate-css/animate.min.css" media="screen" >
        <link rel="stylesheet" href="css/lobipanel/lobipanel.min.css" media="screen" >
        <link rel="stylesheet" href="css/prism/prism.css" media="screen" >
        <link rel="stylesheet" href="css/select2/select2.min.css" >
        <link rel="stylesheet" href="css/main.css" media="screen" >
        <script src="js/modernizr/modernizr.min.js"></script>
    </head>
    <body class="top-navbar-fixed">
        <div class="main-wrapper">

            <!-- ========== TOP NAVBAR ========== -->
  <?php include('includes/topbar.php');?> 
            <!-- ========== WRAPPER FOR BOTH SIDEBARS & MAIN CONTENT ========== -->
            <div class="content-wrapper">
                <div class="content-container">

                    <!-- ========== LEFT SIDEBAR ========== -->
                   <?php include('includes/leftbar.php');?>  
                    <!-- /.left-sidebar -->

                    <div class="main-page">

                     <div class="container-fluid">
                            <div class="row page-title-div">
                                <div class="col-md-6">
                                    <h2 class="title">Subject Creation</h2>
                                
                                </div>
                                
                                <!-- /.col-md-6 text-right -->
                            </div>
                            <!-- /.row -->
                            <div class="row breadcrumb-div">
                                <div class="col-md-6">
                                    <ul class="breadcrumb">
                                        <li><a href="dashboard.php"><i class="fa fa-home"></i> Home</a></li>
                                        <li> Subjects</li>
                                        <li class="active">Create Subject</li>
                                    </ul>
                                </div>
                             
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="container-fluid">
                           
                        <div class="row">
                                    <div class="col-md-12">
                                        <div class="panel">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <h5>Create Subject</h5>
                                                </div>
                                            </div>
                                            <div class="panel-body">
<?php if($msg){?>
<div class="alert alert-success left-icon-alert" role="alert">
 <strong>Well done!</strong><?php echo htmlentities($msg); ?>
 </div><?php } 
else if($error){?>
    <div class="alert alert-danger left-icon-alert" role="alert">
                                            <strong>Oh snap!</strong> <?php echo htmlentities($error); ?>
                                        </div>
                                        <?php } ?>
                                                <form class="form-horizontal" method="post">
                                                    <div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Name</label>
                                                        <div class="col-sm-10">
 <input type="text" name="subjectname" class="form-control" id="default" placeholder="Subject Name" required="required">
                                                        </div>
                                                    </div>
<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Subject Code</label>
                                                        <div class="col-sm-10">
 <input type="text" name="subjectcode" class="form-control" id="default" placeholder="Subject Code" required="required">
                                                        </div>
</div>
<div class="form-group">
                                                        <label for="default" class="col-sm-2 control-label">Units</label>
                                                        <div class="col-sm-10">
 <input type="text" name="units" class="form-control" id="default" placeholder="Unit Code" required="required">
                                                        </div>
                                                    </div>
                                                    


                                                    
                                                    <div class="form-group">
                                                        <div class="col-sm-offset-2 col-sm-2">
                                                            <button type="submit" name="submit" class="btn btn-primary">Create</button>
                                                        </div>
                                                        </form>
                                                        <div class="col-sm-4 text-right">
                                                            <u><i class="text-danger" class="margin-input-bot">.csv file (SubjectName,SubjectCode,Units)</i></u>
                                                        </div>
                                                        <form method="post" enctype="multipart/form-data">
                                                        <div class="col-sm-2 text-right">
                                                                <input type="file" accept=".csv" name="excel" class="margin-input-bot" />
                                                        </div>
                                                        <div class="col-sm-2 text-center">
                                                            <input type="submit" name="import" class="btn btn-info" value="Import" />
                                                        </div>
                                                        </form>
                                                        <div class="col-sm-12 text-right">
                                                            <i class="text-success" class="margin-input-bot"><?php echo $output; ?></i>
                                                        </div>
                                                    </div>


                         <div class="container-fluid">
                           
                        <div class="row">
                            <div class="col-sm-offset-1 col-sm-10">
                                    <div class="col-md-3">
                                            <div class="panel-heading">
                                                <div class="panel-title">
                                                    <!-- <h5><a class="btn btn-success" href="upload-subject-csv.php">Upload Subjects (CSV file)</h5></a> -->
                                                </div>
                                            </div>
                                                </form>

                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col-md-12 -->
                                </div>
                    </div>

                </div>
                <!-- /.content-container -->
            </div>
            <!-- /.content-wrapper -->
        </div>
        <!-- /.main-wrapper -->
        <script src="js/jquery/jquery-2.2.4.min.js"></script>
        <script src="js/bootstrap/bootstrap.min.js"></script>
        <script src="js/pace/pace.min.js"></script>
        <script src="js/lobipanel/lobipanel.min.js"></script>
        <script src="js/iscroll/iscroll.js"></script>
        <script src="js/prism/prism.js"></script>
        <script src="js/select2/select2.min.js"></script>
        <script src="js/main.js"></script>
        <script>
            $(function($) {
                $(".js-states").select2();
                $(".js-states-limit").select2({
                    maximumSelectionLength: 2
                });
                $(".js-states-hide").select2({
                    minimumResultsForSearch: Infinity
                });
            });
        </script>
    </body>
</html>
<?PHP } ?>
